#!/bin/env bash
sed -e "s/tex_offset = Vector2( 0, [-0-9] )/tex_offset = Vector2( 0, -8 )/g" vertical_base.tres > vertical_8.tres
sed -e "s/tex_offset = Vector2( 0, [-0-9] )/tex_offset = Vector2( 0, -32 )/g" vertical_base.tres > vertical_32.tres
