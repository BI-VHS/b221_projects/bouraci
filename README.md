# Bourák Honza

## Demo

Naše demoverze je nyní ke stažení. Zažijte bláznivá dobrodružství v této drobné verzi naší hry.

![Linux](https://gitlab.fit.cvut.cz/BI-VHS/b221_projects/bouraci/-/raw/master/HonzaLinux.zip)

![Windows](https://gitlab.fit.cvut.cz/BI-VHS/b221_projects/bouraci/-/raw/master/HonzaWindows.zip)

Za skoro až triviální obtížnost se omlouváme, ale slibujeme, že výsledný produkt by byl šťavnatější.

Nezapomeňte, vždy je vám ve hře oporou manuál pod klávesou F1.

![](https://i.imgur.com/O6kB0g3.png)
![](https://i.imgur.com/V9qsInS.png)


## Koncept
16ti pixelová prolízačka kobek a jeskyní s pohledem shora, hráč se snaží dostat co nejhlouběji
  
Video ukázka cyklů a dialogů s NPC:  
[![](http://img.youtube.com/vi/l_ZF5pETJZE/0.jpg)](http://www.youtube.com/watch?v=l_ZF5pETJZE "Video ukázka cyklů a dialogů s NPC")

## Scény

- Město na povrchu
- Jeskyně
- Hluboké jeskyně
- Jeskynní město
- Podzemní ruiny
- Peklo

## Vizuál
Pixelová grafika s nízkým rozlišením
Hodně částic a vizuálního smogu (krev a kousky těl, kouř atd. )
"Šejdry" na vylepšení některých grafických efektů (výbuchy a efekty z kouzel)

## NC (Nehratelné charaktery)
Obyvatelé města, dobrodruhové a jiní podivíni v jeskyních. - přátelské
Monstra, pekelníci a jiní obyvatelé jeskyň. - nepřátelské
- Kněz
- Obchodník
- Otec
- Permoník obchodník
- Starý čert

## Aktivity
### NC Aktivity
S přátelskými NC bude možné vést dialogy, plnit pro ně úkoly, nebo obchodovat.
Nepřátelské budou na hráče útočit hned jak ho zahlídnou.

### Hráčské aktivity
- S běžnými NC jednuduchý dialog.  
- Se speciálními NC pokročilý dialog, úkol nebo obchod.  
- Sbíraní a používání zbraní a kouzel.  
- Sbírání zbroje a magických doplňků.  
- Zabíjení potvor.  

## Příběh
Hráč hraje za pastevce koz.  
Prochází vesnicí směrem k pastvě a zastaví ho babka. "Dej si pozor, zase se to tady rojí těma rohatejma, zespod a smrdí tu síra".  
Dojde na pastvu, když v tom se blízký kámen zvedne vyletí kouř a ozve se pisknuti (*vizualne a zvukove zobrazeno*).  
Koza zpozorni a pri druhem pisknuti se ke kameni rozebehne.  
*kamera nasleduje kozu*  
Kámen se přiklopí a vzápětí se to samé stane s dalším kamenem, kam koza zase běží.  
Kus dál je vstup do jeskyně a odtamtud se ozve další písknutí a koza tam vběhne.  
*(tohle by nemuselo být přámo vidět, spíše by hráč viděl poslední kámen a směrem dál by byla jeskyně + třeba stopy kopyt)*
...
Hráč zjistí, že jeskyně je zavřená branou a musí se jít zeptat jak se tam dostat zpět do města. Kněží mu řekne, že někdy v lese bydlí poustevník, který by mohl něco vědět. Pro poustevníka bude muset splnit úkol (přinést husu od selky?) a ten mu potom dá klíč k bránam pekelným. Hráč následovně otevře vrata do jeskyně a může pokračovat.  
...  
Následovalo by několik jeskynních pater, v každém patře by na začátku hráč viděl kozu, ale ta by okamžitě utekla.  
...  
Další patra by mohla obsahovat permoníky. Po splnění questu (zabití obludy co jim žere zásoby nebo chodí po levelu) by ti uvolnili cestu dynamitem.  
Také by zde mohlo být město permoníků. Permoníci využívají peklo pro ohřev vody, která pohání jejich parní mašiny. Čerti se jich nevšímají, protože permoníci jen makají a nemají čas hřešit.  
...  
Poslední patra by bylo peklo, kde by byli čerti, kotle s lidmi a trubky s horkou vodou. Občas by bylo nutné posunout nějakou překážku do proudu vody nebo otočit kohoutem aby šlo projít.  
Zde starý čert pustí pastevce dovnitř, za nějakou úplatu.  
Nakonec pastevec dojde do místnosti s runami a vidí uprostřed kozu. Ta se s efekty změní v obřího ďábla a začne boj.  

## Audio
- Vlastní hudba  
- "Mumlání" či jiná náhrada mluvení  
- Jednoduché zvuky příšer  

## Herní mechaniky
- Držení a používání až dvou zbraní  
- Získávání vybavení  
- Sbírání zkušeností  
- Potvory budou mít speciální vzory pohybu  
- Plnění úkolů a obchodování  

## Fotodokumentace

### Obrazovkavýstřeli

![](https://i.imgur.com/yBJ4d0S.png)
![](https://i.imgur.com/micJM9V.png)
![](https://i.imgur.com/DaiAdDT.png)
![](https://i.imgur.com/kAI26eb.png)

### Mapy

Vesnice
![Vesnice](https://i.imgur.com/HEGHqms.png)

Les
![Les](https://i.imgur.com/OpfrxuK.png)

Jeskyně
![Jeskyně](https://i.imgur.com/OmXfbK0.png)

### Doplňky

![](https://i.imgur.com/LZNgTQn.png)
