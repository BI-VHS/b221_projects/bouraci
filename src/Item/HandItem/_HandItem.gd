extends Item
class_name HandItem

export var bullet_path = "res://src/Projectile/Weapon/Arrow.tscn"
export var inaccuracy = 1
export var cooldown = 0.1 #0 - unlimited, seconds between shots

func use_effect() -> void:
	return
	
func pre_inventory_equip(delta) -> bool:
	var picked = false
	if pickable && Input.is_action_just_released("pick_up"):
		picked = State.equip_hand(self, "none")
	elif pickable && Input.is_action_pressed("pick_up") && Input.is_action_just_pressed("left_click"):
		picked = State.equip_hand(self,"left")
	elif pickable && Input.is_action_pressed("pick_up") && Input.is_action_just_pressed("right_click"):
		picked = State.equip_hand(self,"right")
	return picked
	
