extends KinematicBody2D
class_name Item

var pickable = false
var equipped = false
var just_dropped = false
var just_picked = false

var slot : String = ""

var maxspeed : float = 100
var direction  : Vector2 = Vector2()
var accel : float = 0

var inventory_stats = {
	strength = 0,
	defense = 0,
	agility = 0
}
var hand_stats = {
	strength = 0,
	defense = 0,
	agility = 0
}
func equip_effect() -> void:
	if(slot == "1" || slot == "2" || slot == "3"):
		State.bonus_add(inventory_stats)
	elif(slot == "left" || slot == "right"):
		State.bonus_add(hand_stats)
func unequip_effect() -> void:
	if(slot == "1" || slot == "2" || slot == "3"):
		State.bonus_subtract(inventory_stats)
	elif(slot == "left" || slot == "right"):
		State.bonus_subtract(hand_stats)
func use_effect() -> void:
	pass

# Called when the node enters the scene tree for the first time.
func _ready():
	set_physics_process(false)
	$ZHolder/ItemDescription.visible = false
func pre_inventory_equip(_delta) -> bool:
	return false
func inventory_equip(_delta) -> bool:
	var picked = false
	if pickable && Input.is_action_just_released("pick_up"):
		picked = State.put_in_inventory(self, "none")
	elif pickable && Input.is_action_pressed("pick_up") && Input.is_action_just_pressed("slot_1"):
		picked = State.put_in_inventory(self,"1")
	elif pickable && Input.is_action_pressed("pick_up") && Input.is_action_just_pressed("slot_2"):
		picked = State.put_in_inventory(self,"2")
	elif pickable && Input.is_action_pressed("pick_up") && Input.is_action_just_pressed("slot_3"):
		picked = State.put_in_inventory(self,"3")
	return picked
	
func toss(dir : Vector2, acc : float):
	accel = acc
	direction = dir
	set_physics_process(true)

var picking = false	
func _physics_process(delta):
	if(equipped):
		return
	if(pre_inventory_equip(delta)): 
		return
	if(inventory_equip(delta)): 
		return
	if(picking):
		get_node("ZHolder/ItemDescription").visible = (State.closest_item == self)
		pickable = (State.closest_item == self)
		if(State.closest_item == null):
			State.closest_item = self
		else:
			var curr_min = State.player.instance.global_position.distance_to(State.closest_item.global_position)
			var dist = State.player.instance.global_position.distance_to(global_position)
			if(dist < curr_min):
				State.closest_item = self

	if(accel > 1):
		var velocity = maxspeed * delta * direction * accel
		move_and_slide(velocity)
		if get_slide_count() > 0:
			var collision = get_slide_collision(0)
			if collision != null:
				direction = direction.bounce(collision.normal)
		accel = lerp(accel, 0, delta * 4)
	else:
		accel = 0
		if(!picking):
			set_physics_process(false)

func _on_Player_body_entered(body):
	if body.name == "Player" and not equipped and not just_dropped:
		set_physics_process(true)
		picking = true
func _on_Player_body_exited(body):
	if body.name == "Player":
		if(accel <= 0):
			set_physics_process(false)
		get_node("ZHolder/ItemDescription").visible = false
		just_dropped = false
		picking = false
