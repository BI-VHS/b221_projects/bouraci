extends Item
class_name Consumable

var uses_left : int = 2
var empty_item_path : String = "res://src/Item/Thrown/EmptyVial.tscn"
var use_sound = "res://assets/Sound/SFX/drink.mp3"

func _ready():
	._ready()
	$Sprite.set_frame(uses_left - 1)
	
func drink_effect() -> void:
	pass

func use_effect() -> void:
	if(use_sound != ""):
		State.play_sound(State.player.instance, use_sound)
	drink_effect()
	uses_left -= 1
	if(!uses_left):
		if(empty_item_path == ""):
			State.destroy_inventory_item(self)
		else:
			State.replace_with_item(self,empty_item_path)
	else:
		$Sprite.set_frame(uses_left - 1)
		State.ui.update_inventory()
