extends Consumable
class_name AgilityPotion

func _ready():
	uses_left = 1
func drink_effect() -> void:
	State.change_stat("agility",1)

