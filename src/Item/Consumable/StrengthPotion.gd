extends Consumable
class_name StrengthPotion

func _ready():
	uses_left = 1
func drink_effect() -> void:
	State.change_stat("strength",1)

