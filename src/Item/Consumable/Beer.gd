extends Consumable
class_name Beer

func drink_effect() -> void:
	var effect = {
		strength = 1,
		defense = -1,
		agility = 0
	}
	#State.status_effect(effect, 10)
	State.player.instance.heal(25)
	State.get_drunk(0.65)
	
func _ready():
	empty_item_path = "res://src/Item/Thrown/Pint.tscn"
