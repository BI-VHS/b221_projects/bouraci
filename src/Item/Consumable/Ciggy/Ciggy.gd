extends Consumable
class_name Ciggy

func _ready():
	use_sound = "res://assets/Sound/SFX/ciggy.mp3"
	uses_left = 1
	empty_item_path = "res://src/Item/Consumable/Ciggy/CiggyBurning.tscn"
