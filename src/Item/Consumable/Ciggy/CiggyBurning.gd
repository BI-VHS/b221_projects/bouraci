extends Item
class_name CiggyBurning
	
var butt_path := "res://src/Item/Consumable/Ciggy/CiggyButt.tscn"
	
func _ready():
	inventory_stats = {
		strength = 0,
		defense = 0,
		agility = 1
	}
	yield(get_tree().create_timer(30, false), "timeout")
	$Sprite.animation = "half"
	if(State.ui):
		State.ui.update_inventory()
	yield(get_tree().create_timer(30, false), "timeout")
	if(slot):
		State.replace_with_item(self, butt_path, true)
	else:
		var butt : Item = load(butt_path).instance()
		get_parent().add_child(butt)
		butt.global_transform = global_transform
		queue_free()
