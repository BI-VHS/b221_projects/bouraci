extends Item
class_name Thrown

var projectile_path : Resource = preload("res://src/Projectile/Thrown/EmptyVialProjectile.tscn")

func use_effect():
	var muzzle = State.player.instance.get_node("MuzzleHolder/MuzzleRotator/MuzzleRight")
	State.player.instance.launch_projectile_at_mouse(projectile_path, muzzle)
	State.destroy_inventory_item(self)

func _ready():
	pass
