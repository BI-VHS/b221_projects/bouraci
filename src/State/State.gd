extends Node

#Due to the deep intertwining between UI and player actions,
#EVERYTHING regarding the player will be global. Why not.
class PlayerState:
	
	var instance : Player = null
	
	var alcohol_level : float = 0
	var wobble : float = 0
	
	var left_hand : HandItem = null
	var right_hand : HandItem = null
	
	var slot1 : Item = null #InventoryItem or HandItem
	var slot2 : Item = null
	var slot3 : Item = null
	
	var money : int = 0
	
	var left_hand_bullet_path = null
	var right_hand_bullet_path = null
	
	var stats = {
		strength = 10,
		defense = 10,
		agility = 10
	}
	var bonus = {
		strength = 0,
		defense = 0,
		agility = 0
	}
#Globals
var closest_NPC : NPC = null #for convos
var closest_item : Item = null
var walls : TileMap = null
var player : PlayerState = PlayerState.new()
var ui : UI = null
var music : AudioStreamPlayer = null
var bools = {
	"valley_unlocked" : false
}
#Stats
func bonus_add(var stats : Dictionary):
	player.stats.strength += stats.strength
	player.stats.defense += stats.defense
	player.stats.agility += stats.agility
	player.bonus.strength += stats.strength
	player.bonus.defense += stats.defense
	player.bonus.agility += stats.agility
	ui.update_stats()
func bonus_subtract(var stats : Dictionary):
	player.stats.strength -= stats.strength
	player.stats.defense -= stats.defense
	player.stats.agility -= stats.agility
	player.bonus.strength -= stats.strength
	player.bonus.defense -= stats.defense
	player.bonus.agility -= stats.agility
	ui.update_stats()
	
func change_stat(var stat : String, var amount : int):
	player.stats[stat] += amount
	ui.update_stats()
#Status effect
func status_effect(effect : Dictionary, duration : float) -> void:
	bonus_add(effect)
	var p = player.instance
	yield(get_tree().create_timer(duration, false), "timeout")
	if(p != player.instance):
		return
	bonus_subtract(effect)

var effect_period : float = 1
var old_alcohol_bonus = {
	strength = 0,
	defense = 0,
	agility = 0
}
	
func get_drunk(amount : float) -> void:
	player.alcohol_level += amount
	ui.update_alcohol(player.alcohol_level)
func process_alcohol(delta) -> void:
	if(player.alcohol_level > 0):
		effect_period -= delta
		player.alcohol_level = max(0, player.alcohol_level - delta/30)
		ui.update_alcohol(player.alcohol_level)
		if(effect_period < 0):
			bonus_subtract(old_alcohol_bonus)
			var x = player.alcohol_level
			
			var new_alcohol_bonus = {
				strength = floor(pow(((3.0/2)*x),2.0/3)),
				defense = -floor(pow(((3.0/2)*x),1.0/2)),
				agility = 0 #-floor(pow(((3.0/2)*x),3.0/4))
				#Perhaps slighly messing with the player's speed is lame
			}
			
			#if(x > 3):
			#	player.wobble = log(x-2)
			#else:
			#	player.wobble = 0
			
			#if(x > 5):
			#	Engine.time_scale = max(0.75, 1 - pow(((x-5)/14),2))
			#else:
			#	Engine.time_scale = 1
			bonus_add(new_alcohol_bonus)
			old_alcohol_bonus = new_alcohol_bonus
			effect_period = 1 + effect_period
#Stats
func damage_bonus() -> float:
	return(player.stats.strength)/10.0

func hp_regen() -> float:
	return (10+(player.stats.defense-10) * 2)/5.0
func damage_reduction() -> float:
	return player.stats.defense - 10


func attack_speed_bonus() -> float:
	return (10+(player.stats.agility-10) * 0.75 )/10.0
func speed_bonus() -> float:
	return (10+(player.stats.agility-10)* 0.5 )/10.0

func update_health() -> void:
	var hp = player.instance.hp
	ui.update_health(hp)
#Inventory
func afford(cost : int):
	return (player.money >= cost)
func buy(itemPath : String, cost : int):
	pass

func spawn_inventory_or_ground(new_item_path : String, drop_position : Vector2):
	var item : Item = load(new_item_path).instance()
	var slot : String = ""
	if(item is HandItem):
		slot = free_inventory_slot_or_hand()
	else:
		slot = free_inventory_slot()
	if(slot != ""):
		just_picked = true
		item.just_picked = true
		item.slot = slot
		add_child(item)
		put_in_slot(item, slot)
		item.equip_effect()
		if(slot == "left"):
			player.left_hand_bullet_path = load(item.bullet_path)
		if(slot == "right"):
			player.right_hand_bullet_path = load(item.bullet_path)
	else:
		walls.add_child(item)
		item.global_position = drop_position
		item.toss(Vector2(0,1).rotated(deg2rad(rand_range(-30,30))),5)
	ui.update_inventory()
	
func find_in_inventory(item_path : String) -> Item:
	if(player.left_hand && item_path in player.left_hand.get_script().resource_path):return player.left_hand
	if(player.right_hand && item_path in player.right_hand.get_script().resource_path):return player.right_hand
	if(player.slot1 && item_path in player.slot1.get_script().resource_path):return player.slot1
	if(player.slot2 && item_path in player.slot2.get_script().resource_path):return player.slot2
	if(player.slot3 && item_path in player.slot3.get_script().resource_path):return player.slot3
	return null

func replace_with_item(old_item : Item, new_item_path : String, drop : bool = false):
	var old_slot = old_item.slot
	destroy_inventory_item(old_item)
	var new_item : Item = load(new_item_path).instance()
	if(drop):
		walls.add_child(new_item)
		drop_item(new_item, false)
	else:
		add_child(new_item)
		put_in_slot(new_item, old_slot)
		new_item.slot = old_slot
		new_item.just_picked = true
		new_item.equipped = true
		new_item.visible = false
		just_picked = true
		new_item.equip_effect()
	ui.update_inventory()

func destroy_inventory_item(item : Item) -> void:
	match item.slot:
		"left":
			player.left_hand = null
		"right":
			player.right_hand = null
		"1":
			player.slot1 = null
		"2":
			player.slot2 = null
		"3":
			player.slot3 = null
	item.unequip_effect()
	item.queue_free()
	if(item == closest_item):
		closest_item = null
	ui.update_inventory()

func free_inventory_slot_or_hand() -> String:
	if(!player.left_hand):return "left"
	if(!player.right_hand):return "right"
	if(!player.slot1):return "1"
	if(!player.slot2):return "2"
	if(!player.slot3):return "3"
	return ""

func free_inventory_slot() -> String:
	if(!player.slot1):return "1"
	if(!player.slot2):return "2"
	if(!player.slot3):return "3"
	return ""
func put_in_slot(item: Item, slot: String) -> void:
	match slot:
		"1": player.slot1 = item
		"2": player.slot2 = item
		"3": player.slot3 = item
		"left": player.left_hand = item
		"right": player.right_hand = item
	return
func item_invalid(item : Item) -> bool:
	return (
		player.instance.picking_up 
		|| item == player.right_hand 
		|| item == player.left_hand
		|| item == player.slot1
		|| item == player.slot2
		|| item == player.slot3	
	)
func drop_item(item : Item, toss : bool = true) -> void:
	match item.slot:
		"left":
			player.left_hand = null
		"right":
			player.right_hand = null
		"1":
			player.slot1 = null
		"2":
			player.slot2 = null
		"3":
			player.slot3 = null
	item.unequip_effect()
	remove_child(item)
	player.instance.get_parent().add_child(item)
	item.visible = true
	item.equipped = false
	item.visible = true
	item.just_dropped = true
	item.slot = ""
	var pos = player.instance.get_node("MuzzleHolder/MuzzleRotator/MuzzleRight").global_position
	item.global_position = pos 
	if(toss):
		var mouse_pos = player.instance.get_global_mouse_position()
		item.toss((mouse_pos - pos).normalized() , 50 * damage_bonus())
	ui.update_inventory()

func equip_hand(item : HandItem, slot) -> bool:
	if item_invalid(item):
		return false
	var to_drop = null
	var picked = false
	match slot:
		"left":
			to_drop = player.left_hand
			player.left_hand = item
			player.left_hand_bullet_path = load(item.bullet_path)
			picked = true
		"right":
			to_drop = player.right_hand
			player.right_hand = item
			player.right_hand_bullet_path = load(item.bullet_path)
			picked = true
		"none":
			if(!player.left_hand):
				player.left_hand = item
				slot = "left"
				player.left_hand_bullet_path = load(item.bullet_path)
				picked = true
			elif(!player.right_hand):
				player.right_hand = item
				slot = "right"
				player.right_hand_bullet_path = load(item.bullet_path)
				picked = true
	if(to_drop):
		drop_item(to_drop)
	ui.update_inventory()
	if(picked):
		item.get_parent().remove_child(item)
		add_child(item)
		put_in_slot(item,slot)
		item.just_picked = true
		just_picked = true
		item.equipped = true
		item.visible = false
		item.slot = slot
		item.equip_effect()
		item.global_position = player.instance.global_position
		player.instance.picking_up = 1
		ui.update_inventory()
		
		return true
	else:
		return false

func put_in_inventory(item : Item, slot : String) -> bool:
	if(item_invalid(item)):
		return false
	var to_put = slot
	var to_drop = null
	var picked = false
	match slot:
		"1":
			to_drop = player.slot1
			picked = true
		"2":
			to_drop = player.slot2
			picked = true
		"3":
			to_drop = player.slot3
			picked = true
		"none":
			var free_slot = free_inventory_slot()
			if(free_slot == ""):
				return false
			slot = free_slot
			picked = true
	if(to_drop):
		drop_item(to_drop)
	ui.update_inventory()
	if(picked):
		item.get_parent().remove_child(item)
		add_child(item)
		put_in_slot(item,slot)
		item.equipped = true
		item.visible = false
		item.just_picked = true
		just_picked = true
		item.slot = slot
		item.equip_effect()
		item.global_position = player.instance.global_position
		player.instance.picking_up = 1
		ui.update_inventory()
		return true
	else:
		return false
#Inventory controls
func inventory_controls() -> void:
	if Input.is_action_pressed("drop"):
		if Input.is_action_just_pressed("slot_1"):
			if(player.slot1): drop_item(player.slot1)
		elif Input.is_action_just_pressed("slot_2"):
			if(player.slot2): drop_item(player.slot2)
		elif Input.is_action_just_pressed("slot_3"):
			if(player.slot3): drop_item(player.slot3)
		elif Input.is_action_just_pressed("left_click"):
			if(player.left_hand): drop_item(player.left_hand)
		elif Input.is_action_just_pressed("right_click"):
			if(player.right_hand): drop_item(player.right_hand)
	elif Input.is_action_just_pressed("slot_1"):
		if(player.slot1 && !player.slot1.just_picked): 
			player.slot1.use_effect()
	elif Input.is_action_just_pressed("slot_2"):
		if(player.slot2 && !player.slot2.just_picked): 
			player.slot2.use_effect()
	elif Input.is_action_just_pressed("slot_3"):
		if(player.slot3 && !player.slot3.just_picked): 
			player.slot3.use_effect()
var just_picked = false
func clear_just_picked() -> void:
	if(just_picked == false): return
	if(player.slot1):player.slot1.just_picked = false
	if(player.slot2):player.slot2.just_picked = false
	if(player.slot3):player.slot3.just_picked = false
	if(player.left_hand):player.left_hand.just_picked = false
	if(player.right_hand):player.right_hand.just_picked = false
	just_picked = false
func _process(delta):
	inventory_controls()
	clear_just_picked()
#Scenes
var main_scene = "res://src/Map/MapScenes/TestVillage.tscn"
#Sound
func play_sound(object : Node2D, sound : String, volume : float = 0) -> void:
	var soundplayer = load("res://src/Components/SoundPlayer.tscn").instance()
	walls.add_child(soundplayer)
	soundplayer.global_position = object.global_position
	soundplayer.play_sound(load(sound), volume) 
func play_music(map : String, volume : float = -5) -> void:
	var soundplayer = load("res://src/Components/MusicPlayer.tscn").instance()
	walls.add_child(soundplayer)
	soundplayer.play_sound(map, volume) 
	

#State
func reset_state() -> void:
	closest_NPC = null	
	closest_item = null

func reset_all() -> void:
	player = PlayerState.new()
	closest_NPC = null	
	closest_item = null
#todo todo... tudů tudů tudů... 

#print(State.player.left_hand.get_script().get_path())
	
#var roundstart_state = {
#	left_hand_path = "",
#	right_hand_path = "",
#	slot1 = "",
#	slot2 = "",
#	slot3 = "",
#	money = 0,
#	stats = {
#		strength = 10,
#		defense = 10,
#		agility = 10
#	},
#	bonus = {
#		strength = 0,
#		defense = 0,
#		agility = 0
#	}
#}
#func save_state():
#	roundstart_state.left_hand_path = State.player.left_hand.filename if State.player.left_hand else ""
#	roundstart_state.right_hand_path = State.player.right_hand.get_script().get_path() if State.player.right_hand else ""
#	print(roundstart_state.left_hand_path)
#func load_state(instance : Player):
#	player = PlayerState.new()
#	player.instance = instance
#	if(roundstart_state.left_hand_path):
#		var new_left = load(roundstart_state.left_hand_path).instance() 
#		add_child(new_left)
#		equip_hand(new_left, "left")
#
#	pass
func _ready():
	randomize()
