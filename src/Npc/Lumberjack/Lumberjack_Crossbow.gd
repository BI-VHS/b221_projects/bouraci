extends NPC

func _ready():
	$Dialogue.convo = ({
		1 : {text:"Kušuj, mladej.",
		choices:[
		{text:"Cože?", goto:2, setstate:3}
		]},
		2 : {text:"No řikám, kušuj. Vzadu vpravo za stromem je kuše. Tak si jí vem a kušuj."},
		3 : {text:"Kušuj, mladej. Tady se MAKÁ."}
	})
