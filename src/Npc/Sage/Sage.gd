extends NPC
class_name Sage

func _ready():
	
	var name = pick(["Jindro", "Pepíčku", "Honzíku", "Jeníku", "Jozífku"])
	
	$Dialogue.convo = {
		1 : {text:"Buď zdráv, " + name + ". V této jeskyni žiju už sedmdesát let. Chceš hádanky?",
		choices:[
		{text:"Jistě, dědečku.", goto:2, setstate:2},
		{text:"Starče, neviděl jste tu někde tátovu kozu?", goto:10},
		]},

		2 : {text:"Jsem z Kutné Hory, ...?", 
		choices:[
				{text:"Truhlářův syn.", goto:50, setstate:50},
				{text:"Kominíkův syn.", goto:50, setstate:50},
				{text:"Koudelníkův syn.", goto:3, setstate:3},
		]},
		3 : {text:"Studený máj, ...?", 
		choices:[
				{text:"Byl lásky čas.", goto:50, setstate:50},
				{text:"Ve stodole ráj.", goto:4, setstate:4},
				{text:"Táta přijde z roboty.", goto:50, setstate:50},
		]},
		4 : {text:"Kovářovic kobyla chodí ...?", 
		choices:[
				{text:"Bosa.", goto:5, setstate:5},
				{text:"Po čtyřech.", goto:50, setstate:50},
				{text:"Nahatá.", goto:50, setstate:50},
		]},
		5 : {text:"Kdo se moc ptá, ...?", 
		choices:[
				{text:"Sám do ní padá.", goto:50, setstate:50},
				{text:"V apatyce nekoupí.", goto:6, setstate:6},
				{text:"Tomu není pomoci.", goto:50, setstate:50},
		]},		
		6 : {text:"Jsi šikovný, " + name + ". Tady máš malinovku.", 
		choices:[
				{text:"Děkuji, dědečku.", goto:7, setstate:7, receive:"res://src/Item/Consumable/StrengthPotion.tscn"},
		]},
		7 : {text:"Upaluj, " + name + ".", 
		choices:[]},		
		10 : {text:"Viděl.", choices:[
		{text:"Kde?! Vzal ji Belbetuzár?", goto:11, setstate:100},
		]},
		11 : {text:"Nevzal.", choices:[]},
		50 : {text:"Ještě nejsi připraven, " + name + ".", choices:[]},
		100 : {text:"Buď zdráv, " + name + ". V této jeskyni žiju už sedmdesát let. Chceš hádanku?",
		choices:[
		{text:"Jistě.", goto:2, setstate:2},
		]},
	}
