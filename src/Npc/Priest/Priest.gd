extends NPC
class_name Priest

func _ready():
	$Dialogue.convo = {
		1 : {text:"Pochválen buď Ježíš Kristus.",
		choices:[
		{text:"Až navěky, otče.", goto:2, setstate:2},
		{text:"Neviděls tady někde kozu?", goto:10},
		]},
		2 : {text:"Dlouho jsme tě tu neviděli, Honzíku. Copak tě k nám přivádí?", 
		choices:[
				{text:"Utekla mi koza.", goto:3, setstate:3},
		]},
		3 : {text:"Ach tak. Obávám se, že neutekla, jistě ji polapil démon Belbetuzár když se zase včera otevřelo Peklo.", 
		choices:[
				{text:"Ach ne. Kde ho najdu?", goto:4, setstate:4, set_true:"valley_unlocked", update_quest:"Projdi rokli."},
		]},
		4 : {text:"Ty se chceš setkat s Velkým Mlátičem? Vrchním Démonem a Nejvyšším Zbojníkem? No dobrá. Belbetuzár se skrývá v jeskyni na severu. Projdeš vesnicí, projdeš roklí a budeš tam.", 
		choices:[
				{text:"Děkuji.", goto:5, setstate:6},
		]},
		5 : {text:"Pro všechny případy, zkus si od kováře sehnat něco na sebeobranu. Neboj, zaplatím mu. Máme od Papeže proti démonům dotace.", 
		choices:[]},
		6 : {text:"Bůh tě provázej, Honzíku.", 
		choices:[]},
		10 : {text:"No dovol!"},
	}
