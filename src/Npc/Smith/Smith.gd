extends NPC
class_name Smith

func _ready():
	$Dialogue.convo = {
		1 : {text:"Nazdar, mladej.",
		choices:[
		{text:"Nazdar, kováři. Neviděls tady někde kozu?", goto:2},
		{text:"Ahoj, posílá mě farář. Prej bys mi zvládl dát něco proti loupežníkům?", goto:3, show_if:"valley_unlocked"},
		]},
		2 : {text:"Hele, neviděl. Ale zkus se zeptat pana faráře. Je na východě vesnice.", 
		choices:[]},
		3 : {text:"Jdeš do rokle, jo? Jediný co bych ti nejradši dal je dobrá rada ať zůstaneš hezky tady. " + \
				  "S rohatejma bych si fakt nezahrával. Ale když myslíš...", 
		choices:[
			{text:"Stará hůl postačí, kováři.", goto:4, setstate:4, receive:"res://src/Item/HandItem/Staff.tscn"},
			{text:"Kuše by nebyla?", goto:4, setstate:4, receive:"res://src/Item/HandItem/Crossbow.tscn"},
			{text:"Dej mi svůj nejlepší meč.", goto:4, setstate:4, receive:"res://src/Item/HandItem/Sword.tscn"},
		]},
		4 : {text:"Hodně štěstí, mladej.", 
		choices:[]},

	}
