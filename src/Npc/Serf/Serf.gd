extends "res://src/Npc/_NPC.gd"

const names = ["Pepa", "Honza", "Čeněk", "Luděk", "Luboš", "Ludva", "Venca", "Jarda"]



func _ready():
	
	moves = true
	behavioral_state = pick([STATE_WORK, STATE_WORK, STATE_DRINK, STATE_ERRAND])
	
	match behavioral_state:
		STATE_WORK:
			current_area = pick(work_areas)
		STATE_DRINK:
			current_area = pick(drink_areas)
			if(!current_area):
				current_area = pick(work_areas)
		STATE_ERRAND:
			current_area = pick(errand_areas)
			if(!current_area):
				current_area = pick(drink_areas)
	if(current_area):
		target_pos = current_area.random_point()
	global_position = target_pos
	
	timer.state_cycle = rand_range(0, state_period)
	timer.work_cycle = rand_range(0,work_cycle_period)
	
	var dialogues = []
	var npc_name = pick(names)
	
	dialogues.push_back({ 
			1 : {text:"Ahoj, jak to křupe? Já jsem " + npc_name + ".", 
			choices:[
			{text:"Ahoj, já jsem Honza, syn Oldřichův.", goto:2},
			{text:"Hele, křupe to blbě, ztratil jsem tátovu kozu.", goto:3},
		]},
	2 : {text:"Rád tě poznávám! Tvýho tátu už jsme tu dlouho neviděli, jak to jde?", 
		choices:[
			{text:"No, nic moc, ztratil jsem tátovu kozu.", goto:8},
			{text:"No, nic moc, tátovi se zaběhla koza.", goto:3},
			{text:"No, nic moc, táta ztratil jednu kozu.", goto:6, setstate:7},
		]},
	3 : {text:"Jí tu hledáš, jo? Žádnou kozu jsem tu neviděl. Chceš aspoň cígo?", choices:[
			{text:"Tak jo, no. Dík.", goto:4, receive:"res://src/Item/Consumable/Ciggy/Ciggy.tscn", setstate:5}
		]},
	4 : {text:"Tady máš. Ať to křupe."},
	5 : {text:"Další cígo už nemam, promiň. Hodně štěstí s tou kozou."},
	6 : {text:"Fakt jo? Tak to to s ním jde z kopce. Snad jí najde."},
	7 : {text:"Přeju fotříkovi hodně štěstí s kozou, pozdravuj."},
	8 : {text:"Ha, tak ten tě zabije! Hele, já nic neviděl, ale zkus se zeptat faráře.", 
		choices:[{text:"Proč faráře?", goto:9, setstate:10}]},
	9 : {text:"Nedávno tady chlapovi taky utekla koza a farář ji pak viděl vyběhnout bůhví odkud."},
	10: {text:"No jak jsem řikal, žejo, zkus faráře. Kostel nám nedávno vodvezli, tak teď káže ze svý chaloupky na východě vesnice."}
	})
	dialogues.push_back({
		1 : {text:"Mladej, nemáš drobný?",
		choices:[
		{text:"Nemam nic, promiň.", goto:2, setstate:2},
		{text:"Na co?", goto:3},
		]},
		2 : {text:"Lakomče vošklivej!"},
		3 : {text:"V krámu jsem zastavil prstýnek po tetě, hezkej, "+ 
				  "dostal jsem pár korun a úrok jak Brno."}
	})
	#Doplňující konverzace
	dialogues.push_back({
		1 : {text:"Nejseš vocaď, co!"}
	})
	dialogues.push_back({
		1 : {text:"Vodkaď si?!"}
	})
	dialogues.push_back({
		1 : {text:"Každej ví, že hospůdka   U Tří šutrů je ta nejlepší v okolí."}
	})

	dialogues.push_back({
		1 : {text:"Dneska hrajem proti Dolnímu Křemšínu! Tak nám drž palce, mladej."}
	})
	dialogues.push_back({
		1 : {text:"Vodkaď si?!"}
	})
	dialogues.push_back({
		1 : {text:"Hele mladej, ty vůbec nic nevíš o životě.", 
				choices:[{text:"Co?", goto:2}]},
		2 : {text:"No neni to jen tak, mladej, jednou seš nahoře, jednou dole. " 
				 +"Jednou prohraješ, podruhý zas vyhraješ.",
				choices:[{text:"Cože?", goto:3}]},
		3 : {text:"No první starou sem prohrál. Vzala kluka a "+
				  "zdrhla s nim do Dolního Křemšejna.",
				choices:[{text:"No?", goto:4}]},
		4 : {text:"Ale kamaráde, to ti povim, druhou starou, to sem vyhrál. "+
				  "Přídu domu a na stole dycky klobáska a křeníček, jak to mam rád. "+
				  "Však vona ví."}
	})
	dialogues.push_back({
		1 : {text:"Hele mladej, neskočil bys mi pro jedno hezky vorosený?", choices:[
				{text:"Tak jo.", 
					goto:2, 
					setstate:3},
			]},
	2 : {text:"A máš ňáký?", choices:[
				{text:"Jasně.", goto:4, receive:"res://src/Item/Consumable/AgilityPotion.tscn",
				 give:"Beer", setstate:4},
			]},
	3 : {text:"Tak máš to pivečko?", choices:[
				{text:"Jasně.", goto:4, receive:"res://src/Item/Consumable/AgilityPotion.tscn",
				 give:"Beer", setstate:5},
			]},
	4 : {text:"Dík. Celej den tu chlastám akorát zelenou. Víšty co? Vem si jí.", choices:[]},
	5 : {text:"Dík mladej.", choices:[]},
	})
	if(prob(35)): #Důležité pranostiky a přísloví
		dialogues.push_back({
			1 : {text:"Komu není shůry dáno, v apatyce nekoupí."}
		})
		dialogues.push_back({
			1 : {text:"Kdo jinému jámu kopá, v apatyce nekoupí."}
		})
		dialogues.push_back({
			1 : {text:"Pes, který štěká, v apatyce nekoupí."}
		})
		dialogues.push_back({
			1 : {text:"Co oči nevidí, v apatyce nekoupí."}
		})
		dialogues.push_back({
			1 : {text:"Svatá Lucie noci upije, ale v apatyce nekoupí."}
		})
		dialogues.push_back({
			1 : {text:"Když se dva perou, třetí si jde nakoupit do apatyky."}
		})
		dialogues.push_back({
			1 : {text:"I mistr tesař někdy v apatyce nekoupí."}
		})
		dialogues.push_back({
			1 : {text:"Když ptáčka lapají, v apatyce nekoupí."}
		})
		dialogues.push_back({
			1 : {text:"Darovaného koně v apatyce nekoupíš."}
		})
		dialogues.push_back({
			1 : {text:"Kdo lže, ten krade. A v apatyce nekoupí."}
		})
		dialogues.push_back({
			1 : {text:"Co se v mládí naučíš, v apatyce nekoupíš."}
		})
		dialogues.push_back({
			1 : {text:"Vlk se nažral a koza zůstala v apatyce."}
		})
		dialogues.push_back({
			1 : {text:"Tak dlouho jdou dva se džbánem pro vodu, až prostřední upadne. Nebo tak něco."}
		})
		dialogues.push_back({
			1 : {text:"Kolik hlav máš, tolikrát jsi člověkem."}
		})

	$Dialogue.convo = dialogues[randi() % dialogues.size()]
