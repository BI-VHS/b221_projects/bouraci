extends KinematicBody2D
class_name NPC

#Helpers for dialogues
const text = "text"
const choices = "choices"		
const goto = "goto"				#do NOT set to 0
const setstate = "setstate"		#do NOT set to 0
const give = "give"
const receive = "receive"
const enabled_if = "enabled_if"
const show_if = "show_if"
const set_true = "set_true"
const set_false = "set_false"
const cost = "cost"
const hover_text = "hover_text"
const update_quest = "update_quest"
###############################
#BEHAVIOR STUFF
#todo sort this

#Public:
export var moves : bool = false #Test THOROUGHLY if ever set to true on anyone
var work_type = WORK_LINEAR #None, Static, Random, Linear
var work_area_type = "field"
var drink_area_type = "pub"
var errand_area_types = ["blacksmith", "church"]
var state_period := 45.0
var work_cycle_period := 3
var update_period := 1
var bubble_lifespan := 3
var contact_lifespan := 0.5

var work_messages = ["A jdu makat...", "A teď do práce...", "Chjo..."]
var work_drink_messages = ["Mám padla. Nazdar.", "Jde se na pivko.",
						   "Vožeru se jak dobytek."]
var work_errand_messages = ["Ještě neco zařídit...", "Ještě maličkost..."]
var drink_messages = ["OLÉ, OLE OLE OLÉ!", "KŘEMŠEJN DO HNOJE!",
					  "Ještě do druhý nohy!", "Tak na posilněnou...",
					  "Máte někdo voheň?", "Tekuté zlato...", "Tekutý chléb...",
					  "Takovej podmíráček...", "No tyvole tenkrát...",
					  "Tak ještě poslední.", "[Mohutné říhnutí]",
					  "Pamatujete za mlada?", "Křemšejnu to natřem...",
					  "Rundu panáků!", "JOOO!!! JOOOOO!!!"]
var work_cycle_messages = ["Zase šutr...", "To je makačka...",
							"Lepší než kancelář...", "Už bych šel na pivo...",
							]

#Private:
enum {STATE_WORK, STATE_DRINK, STATE_ERRAND} 
enum {WORK_NONE, WORK_STATIC, WORK_RANDOM, WORK_LINEAR}
var work_areas = []
var drink_areas = []
var errand_areas = []
var timer = {
	"update_cycle":update_period,
	"state_cycle":state_period,
	"work_cycle":work_cycle_period,
	"bubble_timer":0.0,
	"contact_timer":0.0
}
var timer_enable = {
	"update_cycle":true,
	"state_cycle":true,
	"work_cycle":true,
	"bubble_timer":true,
	"contact_timer":true
}
var threshold : int = 2
var path : PoolVector2Array = []
var nav : Navigation2D = null
var current_area : MapArea = null
var target_pos : Vector2 = Vector2(-INF, -INF)

var walking : bool = false
var facing_up = false
var working : bool = false
var behavioral_state : int = STATE_WORK

var maxspeed = 35
var velocity : Vector2 = Vector2()
var current_accel = 0

var talking : bool = false

func prob(var p : float) -> bool:
	return randf() * 100 <= p

func pick(var a : Array): #No type but it just works
	if(!a.size()):
		return null
	return a[randi() % a.size()]


func speech_bubble(s : String) -> void:
	if(talking):
		return
	$ZHolderSpeech/Speech.text = s
	$ZHolderSpeech/Speech.visible = true
	timer.bubble_timer = bubble_lifespan
func get_work_target() -> void:
	match work_type:
		WORK_STATIC:
			return
		WORK_RANDOM:
			target_pos = current_area.random_point()
		WORK_LINEAR:
			target_pos = current_area.next_point(global_position)

func update_behavioral_state() -> void:
	var changed := false
	match behavioral_state:
		STATE_WORK:
			if(working):
				if(timer.state_cycle < 0):
					changed = true
					working = false
					if(prob(75)):
						behavioral_state = STATE_DRINK
						if(prob(75)):
							speech_bubble(pick(work_drink_messages))
					else:
						if(prob(50)):
							speech_bubble(pick(work_errand_messages))
						behavioral_state = STATE_ERRAND
					timer.state_cycle = state_period
				elif(timer.work_cycle < 0):
					if(prob(20)):
						speech_bubble(pick(work_cycle_messages))
					get_work_target()
					timer.work_cycle = work_cycle_period + rand_range(-0.1, 0.1)
					return
			else:
				if target_pos == Vector2(-INF, -INF):
					changed = true
				if(target_pos.distance_to(global_position) < threshold):
					working = true
		STATE_DRINK:
			if(not walking and prob(20)):
				speech_bubble(pick(drink_messages))
			if(timer.state_cycle < 0):
				changed = true
				if(rand_range(0,100) > 25):
					if(prob(50)):
						speech_bubble(pick(work_messages))
					behavioral_state = STATE_WORK
				else:
					if(prob(50)):
						speech_bubble(pick(work_errand_messages))
					behavioral_state = STATE_ERRAND
				timer.state_cycle = state_period
		STATE_ERRAND:
			if(timer.state_cycle < 0):
				changed = true
				if(rand_range(0,100) > 25):
					behavioral_state = STATE_DRINK
				else:
					if(prob(50)):
						speech_bubble(pick(work_messages))
					behavioral_state = STATE_WORK
				timer.state_cycle = state_period
	if(changed):
		match behavioral_state:
			STATE_WORK:
				current_area = pick(work_areas)
			STATE_DRINK:
				current_area = pick(drink_areas)
				if(!current_area):
					current_area = pick(work_areas)
			STATE_ERRAND:
				current_area = pick(errand_areas)
				if(!current_area):
					current_area = pick(drink_areas)
		if(current_area):
			target_pos = current_area.random_point()
		
	var label : String
	match behavioral_state:
			STATE_WORK:
				label = "w/" + str(int(timer.state_cycle)) + "/" + str(int(timer.work_cycle))
			STATE_DRINK:
				label  = "d/" + str(int(timer.state_cycle))
			STATE_ERRAND:
				label = "e/" + str(int(timer.state_cycle))
				
	$ZHolderSpeech/State.text = label
	return

func get_target_path() -> void:
	if(!walking):
		if(target_pos == Vector2(-INF, -INF)):
			return
		if(target_pos.distance_to(global_position) < threshold):
			path = []
			return
		path = nav.get_simple_path(global_position, target_pos, true)
		walking = true
func move_to_target() -> void:
	if(talking): #TODO deccelerate instead
		$AnimatedSprite.animation = "idle"
		return
	if(path.size()):
		if global_position.distance_to(path[0]) < 1:
			path.remove(0)
	if(path.size()):
		var direction = global_position.direction_to(path[0])
		velocity = direction * maxspeed
		if(velocity.y > 0):
			$AnimatedSprite.animation = "walking_down"
			if(velocity.x > 0):
				$AnimatedSprite.flip_h = true
			else:
				$AnimatedSprite.flip_h = false
			facing_up = false
		else:
			$AnimatedSprite.flip_h = false
			$AnimatedSprite.animation = "walking_up"
			facing_up = true

		velocity = move_and_slide(velocity)
	else:
		walking = false
		if facing_up:
			$AnimatedSprite.animation = "idle_up"
		else:
			$AnimatedSprite.animation = "idle"
		
		
func _physics_process(delta : float):
	if(!moves):
		set_physics_process(false)
		return
	
	if(walking):
		timer_enable.update_cycle = false
		timer_enable.state_cycle = false
		timer_enable.work_cycle = false
	else:
		timer_enable.update_cycle = true
		timer_enable.state_cycle = true
		timer_enable.work_cycle = true
		
	for t in timer:
		if(timer_enable[t]):timer[t] -= delta
	if(timer.update_cycle < 0):
		update_behavioral_state()
		get_target_path()
		timer.update_cycle = update_period
	if(timer.bubble_timer < 0 and not talking):
		$ZHolderSpeech/Speech.visible = false
	move_to_target()


##############################
func prepare_navigation() -> void:
	nav = get_parent().get_parent()
	#Fetch areas
	var areas = get_parent().get_node("Areas").get_children()
	for a in areas:
		if(a.area_name == work_area_type):
			work_areas.push_back(a)
		if(a.area_name == drink_area_type):
			drink_areas.push_back(a)
		if(a.area_name in errand_area_types):
			errand_areas.push_back(a)

func prepare_animation() -> void:
	yield(get_tree().create_timer(rand_range(0,1)), "timeout")
	$AnimatedSprite.playing = true
	if($AnimatedSprite.frames.get_frame_count("idle")):
		$AnimatedSprite.frame = randi() % $AnimatedSprite.frames.get_frame_count("idle")

func _process(delta):
	if((talking && timer.contact_timer < 0) || (!moves && talking)):
		$Dialogue.visible = (State.closest_NPC == self)
		if(State.closest_NPC == null):
			State.closest_NPC = self
		else:
			var curr_min = State.player.instance.global_position.distance_to(State.closest_NPC.global_position)
			var dist = State.player.instance.global_position.distance_to(global_position)
			if(dist < curr_min):
				State.closest_NPC = self

func _ready():
	prepare_navigation()
	prepare_animation()

###############################################################

func _on_AreaEnter_body_entered(body):
	if body.name == "Player":
		timer.contact_timer = 0.1
		talking = true
		$ZHolderSpeech/Speech.visible = false
		timer.bubble_timer = 0
		$Dialogue.update_dialogue($Dialogue.convo_state)

func _on_AreaExit_body_exited(body):
	if body.name == "Player":
		talking = false
		$Dialogue.visible = false

