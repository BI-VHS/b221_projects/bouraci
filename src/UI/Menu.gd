extends Control
func _ready():
	pass # Replace with function body.
func _on_DebugMapLoad_pressed(ID):
	print($Panel/DebugMapLoad.get_popup().get_item_text(ID), " pressed")
func _on_Hraj_pressed():
	State.main_scene = "res://src/Map/MapScenes/Demo/1_Village.tscn"
	#State.main_scene = "res://src/Map/MapScenes/Demo/2_Valley.tscn"
	#State.main_scene = "res://src/Map/MapScenes/Demo/3_Cave.tscn"	
	get_tree().change_scene("res://src/Game.tscn")
func _on_Konec_pressed():
	get_tree().quit()
func _on_TestMap_pressed():
	State.main_scene = "res://src/Map/MapScenes/Misc/TestMap.tscn"
	get_tree().change_scene("res://src/Game.tscn")
func _on_NoTut_pressed():
	State.main_scene = "res://src/Map/MapScenes/Demo/1_VillageNoTut.tscn"
	get_tree().change_scene("res://src/Game.tscn")
