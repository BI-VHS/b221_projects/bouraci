extends CanvasLayer
class_name UI

var dead : bool = false
var help : bool = false
var was_stats_visible : bool = false
var info : bool = false
var info_index : int = 0
var info_array : PoolStringArray = []

func _ready():
	State.ui = self
	$Help.bbcode_text = help_text.intro

func update_health(hp) -> void:
	$TopPanel/HPBar.value = hp
	if(hp <= 0):
		get_tree().paused = true
		$Dead.visible = true
		dead = true

func update_alcohol(alcohol) -> void:
	$TopPanel/DrunkBar.value = alcohol

func update_quest(quest) -> void:
	$TopPanel/QuestText.text = quest
	pass

func update_location(location) -> void:
	$TopPanel/LevelText.text = location
	pass

func str_sgn(x : int) -> String:
	var sgn = "" if x < 0 else "+"
	return sgn + str(x)
func update_stats() -> void:
	var s = str(State.player.stats.strength) + "(" + str_sgn(State.player.bonus.strength) + ")"
	var d = str(State.player.stats.defense) + "(" + str_sgn(State.player.bonus.defense) + ")"
	var a = str(State.player.stats.agility) + "(" + str_sgn(State.player.bonus.agility) + ")"
	var text = \
"""[table=2][cell]Síla:[/cell][cell]""" + s + """[/cell]
[cell]Odolnost:[/cell][cell]""" + d + """[/cell]
[cell]Hbitost:[/cell][cell]""" + a + """[/cell]
[/table][color=grey]"""
#Počet zabití: 123456
#Vypitých piv: 45678"""
	$Stats/Stats.bbcode_text = text

var sprites = []

func delete_sprites():
	for n in sprites:
		n.get_parent().remove_child(n)
		n.queue_free()
	sprites = []
	
var slots = ["LeftHand", "RightHand", "Slot1", "Slot2", "Slot3"]

var slot_pairs = [
	{ node_name= "LeftHand", variable_name= "left_hand" },
	{ node_name= "RightHand", variable_name= "right_hand" },
	{ node_name= "Slot1", variable_name= "slot1" },
	{ node_name= "Slot2", variable_name= "slot2" },
	{ node_name= "Slot3", variable_name= "slot3" },
]

func update_inventory() -> void:
	var offset = Vector2(24,24)
	delete_sprites()
	for s in slots:
		var slot : RichTextLabel = get_node("Inventory/" + s)
		slot.visible_characters = -1
		
	$Inventory/Money.bbcode_text = "[color=#C0C0C0]" + str(State.player.money) + "s[/color]"
	var slot_node : Node = null
	var text_node : RichTextLabel = null
	var sprite_node : AnimatedSprite = null
	for s in slot_pairs:
		slot_node = State.player.get(s.variable_name)
		if(slot_node):
			sprite_node = slot_node.get_node("Sprite").duplicate()
			sprite_node.position = Vector2(0,0)
			sprites.append(sprite_node)
			text_node = get_node("Inventory/" + s.node_name)
			text_node.add_child(sprite_node)
			text_node.visible_characters = 0
			sprite_node.position += offset
			sprite_node.scale *= 2
		
func show_info(text_array : PoolStringArray):
	State.player.instance.reset_input()
	$PauseRect.visible = true
	get_tree().paused = true
	$Info.visible = true
	info_array = text_array
	$Info.bbcode_text = info_array[0] + """
	
[color=grey]Pro pokračování stiskni mezerník."""
	info_index = 0
	info = true

func _process(delta):
	if(dead):
		if(Input.is_action_just_pressed("space")):
			get_tree().paused = false
			#get_tree().reload_current_scene()
			State.reset_all()
			get_tree().change_scene("res://src/UI/Menu.tscn")

	elif(info):
		if(Input.is_action_just_pressed("space")):
			info_index += 1
			if(info_index == info_array.size()):
				$PauseRect.visible = false
				State.play_sound(State.player.instance, "res://assets/Sound/SFX/pause.wav")
				get_tree().paused = false
				$Info.visible = false
				info = false
				State.player.instance.reset_input()
			else:
				State.play_sound(State.player.instance, "res://assets/Sound/SFX/pause.wav")
				$Info.bbcode_text = info_array[info_index] + """
				
[color=grey]Pro pokračování stiskni mezerník."""
	elif(help):
		if(Input.is_action_just_pressed("space") || Input.is_action_just_pressed("help")):
			State.play_sound(State.player.instance, "res://assets/Sound/SFX/pause.wav")
			get_tree().paused = false
			help = false
			$PauseRect.visible = false
			$Help.visible = false
			$Stats.visible = was_stats_visible
	elif(Input.is_action_just_pressed("stats")):
		$Stats.visible = !$Stats.visible
	elif(Input.is_action_just_pressed("debug")):
		$DebugMenu.visible = !$DebugMenu.visible
	elif(Input.is_action_just_pressed("help")):
			State.player.instance.reset_input()
			get_tree().paused = true
			State.play_sound(State.player.instance, "res://assets/Sound/SFX/pause.wav")
			help = true
			$Help.bbcode_text = help_text.intro
			was_stats_visible = $Stats.visible
			$Stats.visible = true
			$PauseRect.visible = true
			$Help.visible = true
			

			
var help_text = {
	intro = \
"""Najeď na libovolnou část rozhraní pro zobrazení příslušné nápovědy.

[color=grey]Pro pokračování stiskni mezerník.""",

	stats = \
"""Toto je panel vlastností tvé postavy.

[color=yellow]Síla[/color] ovlivňuje poškození, které tvé zbraně udělují. [color=yellow]Odolnost[/color] snižuje příchozí poškození a zrychluje obnovu tvých životů. [color=yellow]Hbitost[/color] ovlivňuje rychlost tvého pohybu a tvých útoků.

Číslo v závorce označuje, jakým způsobem současně držené předměty ovlivňují danou vlastnost. Záštita meče ti jistě pomůže zabránit nejednomu příchozímu zásahu, zato těžká palice zajisté tvé hbitosti neprospěje. Předměty, které nedržíš v ruce, tvé schopnosti neovlivňují.

Během hry lze tento panel [color=yellow]zobrazit[/color] či skrýt [color=yellow]pomocí tlačítka C[/color].

[color=grey]Pro pokračování stiskni mezerník.""",

	inventory = \
"""Toto je tvůj inventář.

Do horních třech přihrádek lze umístit vše, co v tomto světě najdeš. Spodní dvě vlevo představují tvé ruce a můžeš v nich držet zbraně nebo štíty. Kolonka vpravo dole představuje tvůj současný finanční obnos, uvedený ve stříbrných tolarech.

[color=yellow]Zbraně[/color] použiješ pomocí [color=yellow]tlačítek myši.[/color] Levým tlačítkem myši udeříš levou rukou, pravým tlačítkem pravou. [color=yellow]Předměty[/color] v horních přihrádkách které lze nějak okamžitě použít (například lektvary) použiješ [color=yellow]stisknutím příslušného čísla[/color] na klávesnici, tedy stisknutím klávesy 1 vypiješ lektvar v první přihrádce.

Předměty [color=yellow]sbíráš pomocí klávesy E[/color]. Při stisknutí klávesy E v blízkosti předmětu bude předmět umístěn do první volné ruky či přihrádky. Předměty lze rovněž dát [color=yellow]do tebou zvolené ruky[/color] či přihrádky tak, že [color=yellow]podržíš klávesu E a stiskneš příslušnou klávesu či tlačítko myši[/color]. Lze rovněž rychle vzít do ruky zbraň z inventáře tak, že podržíš klávesu příslušné přihrádky a stiskneš tlačítko myši. Předměty lze také [color=yellow]pohodit[/color] na zem tak, že podržíš klávesu [color=yellow]Ctrl[/color] a stiskneš příslušnou klávesu či tlačítko myši.  

[color=grey]Pro pokračování stiskni mezerník.""",

	topbar = \
"""V horním panelu nalezneš současný úkol, ukazatel životů a tvou pozici ve světě.

Tvůj zobrazený [color=yellow]úkol[/color] je tvůj hlavní (ale jistě ne jediný) úkol v danou chvíli. Většinou se do něj nemusíš hrnout a můžeš v klidu prozkoumávat svět. Ostatní úkoly si musíš pamatovat sám! 

Tvůj [color=yellow]ukazatel životů[/color] je vcelku jednoduchý - jakmile vyprchá, je po tobě. Životy se ti obnovují samy, ale alespoň zpočátku pomalu. Svou pasivní obnovu životů můžeš vylepšit zvýšením své odolnosti. Okamžitou obnovu části životů ti zajistí některé nápoje, například pivo.

Ukazatel [color=yellow]pozice ve světě[/color] ti napíše, ve které vesnici, lese či jeskyni apod. právě jsi. Tyto názvy používají postavy ve světě, tak si je pamatuj. 

[color=grey]Pro pokračování stiskni mezerník."""
	
}

func _on_TopPanel_mouse_entered():
	$Help.bbcode_text = help_text.topbar
func _on_Stats_mouse_entered():
	$Help.bbcode_text = help_text.stats
func _on_Inventory_mouse_entered():
	$Help.bbcode_text = help_text.inventory

