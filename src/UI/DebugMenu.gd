extends Panel

#Stats
func _on_StrPlus_pressed():
	State.change_stat("strength",1)
func _on_DefPlus_pressed():
	State.change_stat("defense",1)
func _on_AgiPlus_pressed():
	State.change_stat("agility",1)
func _on_StrMinus_pressed():
	State.change_stat("strength",-1)
func _on_DefMinus_pressed():
	State.change_stat("defense",-1)
func _on_AgiMinus_pressed():
	State.change_stat("agility",-1)
func _on_HpPlus_pressed():
	State.player.instance.heal(10)
func _on_HpMinus_pressed():
	State.player.instance.take_damage(10)

#Items
#todo

#Enemies
#todo

#NPC
#todo
