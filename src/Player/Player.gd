extends KinematicBody2D
class_name Player
func get_class(): return "Player"

const MAXSPEED = 180
const ACCEL = MAXSPEED * 10
const FRICTION = 10

var motion : Vector2 = Vector2()
var muzzle_dist

export var max_hp = 100
export var hp_regen = 0

var hp = max_hp
var picking_up = 0

var left_hand_cooldown = 0
var right_hand_cooldown = 0

var since_last_damage = 0.0

const UP_MASK = 0b00001000
const RIGHT_MASK = 0b00000100
const DOWN_MASK = 0b00000010
const LEFT_MASK = 0b00000001

var input_mask = 0b00000000

func shoot(hand):
	
	var bullet_path
	var inaccuracy

	var muzzle
	var mouse_pos = get_global_mouse_position()
	$MuzzleHolder/MuzzleRotator.look_at((mouse_pos))
	if(mouse_pos.distance_to($MuzzleHolder.global_position) <= muzzle_dist):
		return
	if(hand == "left"):
		if(!State.player.left_hand):
			return
		if(left_hand_cooldown):
			return
		bullet_path = State.player.left_hand_bullet_path
		inaccuracy = State.player.left_hand.inaccuracy
		muzzle = $MuzzleHolder/MuzzleRotator/MuzzleLeft
	else:
		if(!State.player.right_hand):
			return
		if(right_hand_cooldown):
			return
		bullet_path = State.player.right_hand_bullet_path
		inaccuracy = State.player.right_hand.inaccuracy
		muzzle = $MuzzleHolder/MuzzleRotator/MuzzleRight
	if(!bullet_path):
		return
	launch_projectile_at_mouse(bullet_path,muzzle)
	if(hand == "left"):
		left_hand_cooldown = State.player.left_hand.cooldown
	else:
		right_hand_cooldown = State.player.right_hand.cooldown

func launch_projectile_at_mouse(bullet_path, muzzle):
	var mouse_pos = get_global_mouse_position()
	$MuzzleHolder/MuzzleRotator.look_at((mouse_pos))
	var bullet = bullet_path.instance()
	bullet.damage *= State.damage_bonus()
	get_parent().add_child(bullet)
	bullet.global_position = muzzle.global_position
	bullet.look_at(mouse_pos)
	# offset = muzzle holder Y relative to player
	var offset = $MuzzleHolder.position.y
	bullet.global_position.y -= offset
	bullet.get_node("AnimatedSprite").global_position.y += offset
	bullet.velocity = (mouse_pos - muzzle.global_position)

func hit(bullet : Projectile):
	var damage = max(0, bullet.damage - State.damage_reduction() )
	
	var kb_damage
	if(bullet.knockback>= 0):
		kb_damage = max(0, bullet.knockback - State.damage_reduction() )
	else:
		kb_damage = bullet.knockback + State.damage_reduction() 
	knockback += (bullet.velocity.normalized() * bullet.velocity_scale) * (3*kb_damage)
	
	if(damage):
		motion *= max(0.1,(1 - (sqrt(damage)*20)/100))
		take_damage(bullet.damage)

func take_damage(damage):
	damage = max(0, damage - State.damage_reduction() )
	_add_shake(damage/20.0)
	since_last_damage = 0
	State.play_sound(self, "res://assets/Sound/SFX/player_hit.mp3", damage - 10)
	hp -= damage
	State.update_health()
	
func heal(amount):
	hp = min(hp + amount, 100)
	State.update_health()

func _ready():
	State.player.instance = self
	muzzle_dist = global_position.distance_to($MuzzleHolder/MuzzleRotator/MuzzleLeft.global_position)

#Input variables
var direction : Vector2 = Vector2()
var input_pick_up : bool = false
var input_shoot_left : bool = false
var input_shoot_right : bool = false

var reset_input = false

func reset_input() -> void:
	input_pick_up = false
	input_shoot_left = false
	input_shoot_right = false
	reset_input = true
	move_left = 0
	move_right = 0
	move_up = 0
	move_down = 0

var move_left = 0
var move_right = 0
var move_up = 0
var move_down = 0
func _unhandled_input(event) -> void:
	if(event is InputEventKey):
		if(event.is_echo()):
			return
		if event.is_action_pressed("left"):
			move_left = 1
			return
		if event.is_action_released("left"):
			move_left = 0
			return
		if event.is_action_pressed("right"):
			move_right = 1
			return
		if event.is_action_released("right"):
			move_right = 0
			return
		if event.is_action_pressed("up"):
			move_up = 1
			return
		if event.is_action_released("up"):
			move_up = 0
			return
		if event.is_action_pressed("down"):
			move_down = 1
			return
		if event.is_action_released("down"):
			move_down = 0
			return
		if event.is_action_pressed("pick_up"):
			input_pick_up = true
			return
		if event.is_action_released("pick_up"):
			input_pick_up = false
			return
	elif(event is InputEventMouse):
		if(event.is_action_pressed("left_click") && !input_pick_up): 
			input_shoot_left = true
		if(event.is_action_released("left_click") && !input_pick_up): 
			input_shoot_left = false
		if(event.is_action_pressed("right_click") && !input_pick_up): 
			input_shoot_right = true
		if(event.is_action_released("right_click") && !input_pick_up): 
			input_shoot_right = false
			
func set_animation() -> void:
	var new_animation = "idle"
	if(direction.x > 0):
		new_animation = "running_right"
	elif(direction.x < 0):
		new_animation = "running_left"
	if(direction.y > 0):
		new_animation = "running_down"
	elif(direction.y < 0):
		new_animation = "running_up"
	if($AnimatedSprite.animation != new_animation):
		$AnimatedSprite.animation = new_animation

var drunk_rotation_degrees = 0.0
var drunk_rotation = 0.0

var knockback := Vector2()
func _process_knockback(delta):
	if(abs(knockback.length()) > 10):
		knockback = knockback * 0.25 * (0.01667 / delta)
	else:
		knockback = Vector2()

const decay = 3
const max_offset = Vector2(7.5, 5) 
const max_roll = 0.1 
var shake = 0.0 
var shake_power = 2 
func _add_shake(amount):
	shake = min(shake + amount, 1.0)
func _process_screenshake(delta):
	if shake:
		shake = max(shake - decay * delta, 0)
		var amount = pow(shake, shake_power)
		$Camera2D.rotation = max_roll * amount * rand_range(-1, 1)
		$Camera2D.offset.x = max_offset.x * amount * rand_range(-1, 1)
		$Camera2D.offset.y = max_offset.y * amount * rand_range(-1, 1)

func _physics_process(delta):
	#hp regen
	since_last_damage += delta
	if(hp < 100):
		if(since_last_damage > 10):
			hp += State.hp_regen() * 10 * delta
		elif(since_last_damage > 6):
			hp += State.hp_regen() * 5 * delta
		else:
			hp += State.hp_regen() * delta
		hp = min(hp, 100)
		State.update_health()
	#misc
	State.process_alcohol(delta)
	_process_knockback(delta)
	_process_screenshake(delta)
	var speed_bonus = State.speed_bonus()
	$AnimatedSprite.speed_scale = speed_bonus	

	#shootin'
	if(input_shoot_left): 
		shoot("left")
	if(input_shoot_right): 
		shoot("right")
	
	direction = Vector2()
	direction.x = move_right - move_left
	direction.y = move_down - move_up
	
	if(direction.x < -1): direction.x = -1
	elif(direction.x > 1): direction.x = 1
	if(direction.y < -1): direction.y = -1
	elif(direction.y > 1): direction.y = 1
	
	if(direction.x):
		motion.x += direction.x * ACCEL * speed_bonus * delta 
	else:
		motion.x = lerp(motion.x,0,FRICTION * speed_bonus * delta) * float((abs(motion.x) > 10))
	if(direction.y):
		motion.y += direction.y * ACCEL * speed_bonus * delta
	else:
		motion.y = lerp(motion.y,0,FRICTION * speed_bonus * delta) * float((abs(motion.y) > 10))


	var a = motion.angle()
	motion.y = clamp(motion.y, - MAXSPEED * speed_bonus * abs(sin(a)), MAXSPEED * speed_bonus * abs(sin(a)))
	motion.x = clamp(motion.x, -MAXSPEED * speed_bonus * abs(cos(a)), MAXSPEED * speed_bonus * abs(cos(a)))

	if(motion.length() < 15):
		motion = Vector2()

#	if(State.player.wobble):
#		drunk_rotation_degrees = sin(OS.get_unix_time()* State.player.wobble) * 2.5 * State.player.wobble
#		drunk_rotation = deg2rad(drunk_rotation_degrees)
#		motion = motion.rotated(drunk_rotation)
	motion += knockback
	motion = move_and_slide(motion)
	set_animation()

func _process(delta):
	picking_up = 0
	left_hand_cooldown = max(left_hand_cooldown - delta * State.attack_speed_bonus(),	0)
	right_hand_cooldown = max(right_hand_cooldown - delta * State.attack_speed_bonus(),	0)
