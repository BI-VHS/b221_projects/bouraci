extends "res://src/Enemy/_Enemy.gd"

func _ready():
	bullet_type = "res://src/Projectile/Enemy/Zombie.tscn"
	bullet_path = load(bullet_type)

	drop_table = [
	["res://src/Item/HandItem/Sword.tscn",10],
	["res://src/Item/Consumable/Beer.tscn",25],
	["res://src/Item/Consumable/DefensePotion.tscn",25],
	["res://src/Item/Consumable/StrengthPotion.tscn",20],
	]
