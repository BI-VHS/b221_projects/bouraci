extends AnimatedSprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"



signal current_animation_finished(anim_name)

func _on_animation_finished() -> void:
	emit_signal("current_animation_finished", animation)

func _on_current_animation_finished(anim_name: String) -> void:
	if anim_name == "attack_down" or anim_name == "attack_up":
		get_parent().get_parent().get_parent().get_parent().spawn_effect(global_position)	

func _ready():
	connect("animation_finished", self, "_on_animation_finished")
	connect("current_animation_finished", self, "_on_current_animation_finished")
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
