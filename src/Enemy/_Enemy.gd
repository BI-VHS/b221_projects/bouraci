extends KinematicBody2D
class_name Enemy
#Mente Tohle 
export var FOV_range = 1
export var maxspeed = 100
export var accel = 2
export var min_distance = 20
export var distance_tolerance = 1.5
export var shoot_range = 100
export var touch_damage = 10
export var touch_range = 1
export var bullet_type = "res://src/Projectile/_Projectile.tscn"
export var shoot_cooldown = 1
export var max_hp = 100
export var hp_regen = 0

export var predict_shoot = false
export var predict_move = false

export var hit_sound = ""
export var death_sound = ""

var bullet_path

signal aggro
signal death

##Vase API:
var sprite_state = 0 # 0 - Nahoru, 1 - Dolu
var moving_animation = 1 # Kdyz 1, hraje moving animace, kdyz 0 tak ne
func custom_behavior(): pass # Hrajte si
# Var chasing - Jestli honi hrace
# Var in_range - Hrac je v minimalni vzdalenosti
# Var searching - Hleda hrace kdyz je ve vzdalenosti ale neni raycast

# Pro zmenu utoku:
#	- nactete jiny bullet_path
#		- bullet_path = load("Src/novy_mega_utok")
#	- zmente shoot_range, shoot_cooldown
# Zjisteni vzdalenosti k hraci:
#
#	var dist = global_position.distance_to(player.global_position)
#
func get_class(): return "Enemy"
var cooldown  = 0
var hp = max_hp


var current_accel = 0
var velocity = Vector2.ZERO
var path = []
var THRESHOLD  = 8
var period = 25
var nav = null
var player = null
var chasing = false
var searching = false
var in_range = false



func prob(var p : float) -> bool:
	return randf() * 100 <= p

var drop_table = []

func drop_items():
	for item in drop_table:
		if(prob(item[1])):
			var new_item : Item = load(item[0]).instance()
			get_parent().add_child(new_item)
			new_item.global_position = global_position
			new_item.toss(Vector2(0,1).rotated(deg2rad(rand_range(0,360))), rand_range(50,75))
func shoot():
	if(!bullet_path):
		return
	if(cooldown):
		return
	var dist = global_position.distance_to(player.global_position)
	if(dist > shoot_range):
		return
	moving_animation = 0

	var bullet : Projectile = bullet_path.instance()
	bullet.hit_player = true
	get_parent().add_child(bullet)

	var beta = 0.0
	var target_pos =player.global_position
	if(predict_shoot):
		var player_velocity = player.motion.length()
		var bullet_velocity = bullet.initial_speed
		var alpha = (player.motion).angle_to(global_position - player.global_position)
		beta = asin((sin(alpha) * player_velocity) / bullet_velocity)
			
	#   b = player_velocity * t
	#  P______F
	#   \α   /
	#	 \β / a = bullet_velocity * t  
	#     \/  (linear but you could mess with the accel)
	#	   E	
	# P... player
	# E... enemy
	# F... Player future position - our target
	# sinα/a = sinβ/b
	# sinα/(bullet_velocity * t) = sinβ/(player_velocity * t)
	# β = arcsin((sinα*player_velocity)/bullet_velocity)
	else:
		target_pos = player.global_position

	var muzzle = $MuzzleHolder/MuzzleRotator/Muzzle
	$MuzzleHolder/MuzzleRotator.look_at(target_pos)
	bullet.velocity = target_pos - $MuzzleHolder/MuzzleRotator.global_position
	bullet.global_position = muzzle.global_position
	bullet.rotation = bullet.velocity.angle()
	# offset = muzzle holder Y relative to enemy
	var offset = $MuzzleHolder.position.y
	bullet.global_position.y -= offset
	bullet.get_node("AnimatedSprite").global_position.y += offset
	bullet.velocity = (target_pos - bullet.global_position).normalized().rotated(beta)
	
	
	
	if bullet.velocity.y > 0: 
		$AnimatedSprite.animation = "attack_down"
	else:
		$AnimatedSprite.animation = "attack_up"
	bullet.get_node("AnimatedSprite").global_position.y -= 6 
	cooldown = shoot_cooldown

func hit(bullet : Projectile):
	knockback += (bullet.velocity.normalized() * bullet.velocity_scale) * (5*bullet.knockback) * (100/max_hp)
	current_accel = max(current_accel / (bullet.knockback / 1.75), 0)
	take_damage(bullet.damage)

func take_damage(damage : float):
	hp -= damage
	$HPBar.value = hp

	if(hp < 0):
		drop_items()
		emit_signal("death")
		State.play_sound(self,death_sound)
		queue_free()
	State.play_sound(self,hit_sound)

func _ready():
	bullet_path = load(bullet_type)
	nav = get_parent().get_parent()
	player = get_parent().get_node("Player")
	hp = max_hp
	$AnimatedSprite.animation = "idle"
	$AreaFOV/FOV.scale *= FOV_range
	$HPBar.max_value = max_hp
	$HPBar.value = max_hp
	$AreaHit/Hit.scale *= touch_range

var knockback := Vector2()
func _process_knockback(delta):
	if(knockback.length() > 10):
		knockback = knockback * 0.25 * (0.01667 / delta)
	else:
		knockback = Vector2()


func _physics_process(delta):
	hp = min(hp + hp_regen, max_hp)
	
	if(chasing):
		shoot()
	custom_behavior()
	
	_process_knockback(delta)
	move_and_slide(knockback)
	
	cooldown = max(cooldown - 1,0)
	if (searching && !chasing):
		var space_state = get_world_2d().direct_space_state
		var result = space_state.intersect_ray(global_position, player.global_position, [self])	
		if(result && result["collider"].get_class() == "Player"): 
			get_target_path()
			chasing = true
			emit_signal("aggro")
			$HPBar.visible = true
		
	if (!chasing):
		return

	var dist = global_position.distance_to(player.global_position)
	if ((dist) < min_distance) || (in_range && dist < min_distance * distance_tolerance):
		var space_state = get_world_2d().direct_space_state
		var result = space_state.intersect_ray(global_position, player.global_position, [self])	
		if((result && result["collider"].get_class() == "Player") || dist < 5): 
			in_range = true
			current_accel -= 2 * accel * delta
			current_accel = max(current_accel, 0)
			path.resize(0)
			return
	
	moving_animation = 1
	in_range = false
	current_accel += accel * delta
	current_accel = min(current_accel, 1)

	if path.size():
		move_to_target()
	period -= 1
	if(!period || path.size() == 0):
		period = 25
		get_target_path()
		move_to_target()

func move_to_target():
	if global_position.distance_to(path[0]) < THRESHOLD:
		path.remove(0)
	if(path.size()):
		var direction = global_position.direction_to(path[0])
		velocity = direction * maxspeed

		if(moving_animation):
			if(velocity.y > 0): $AnimatedSprite.animation = "down"
			else: $AnimatedSprite.animation = "up"
		

		
		velocity = move_and_slide(velocity * current_accel)
func get_target_path():
	if(predict_move):
		var dist = global_position.distance_to(player.global_position)
		var player_maxspeed = 180
		var alpha = (player.motion).angle_to(global_position - player.global_position)
		var beta = asin((sin(alpha) * player_maxspeed) / maxspeed)
		var gamma = 180 - alpha - beta
		var time = (sin(beta)/sin(gamma)) * (dist/player_maxspeed)
		path = nav.get_simple_path(global_position, player.global_position + player.motion * time, true)
		if(!path.size()):
			path = nav.get_simple_path(global_position, player.global_position, true)
	else:
		path = nav.get_simple_path(global_position, player.global_position, true)

func _on_AreaFOV_body_entered(body):
	if(chasing):
		return
	if(body.get_class() == "Player"):
		searching = true
	
func _on_AreaFOV_body_exited(body):
	if(body.get_class() == "Player"):
		searching = false

func _on_AreaFOV_area_entered(area):
	if(chasing):
		return
	if(area.get_parent().get_class() == "Bullet"):
		get_target_path()
		chasing = true
		emit_signal("aggro")
		$HPBar.visible = true
		
func _on_AreaHit_body_entered(body):
	if(!touch_damage):
		return
	if(body.get_class() == "Player"):
		body.take_damage(touch_damage)
