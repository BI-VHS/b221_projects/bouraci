extends Projectile

func custom_effect(body):
	var debuff = {
		strength = 0,
		defense = 0,
		agility = -2
	}
	State.status_effect(debuff, 10)
