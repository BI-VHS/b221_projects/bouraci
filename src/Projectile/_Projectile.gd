extends KinematicBody2D
class_name Projectile
func get_class(): return "Bullet"

export var hit_player = false
export var bullet_range =2.0
export var init_coeff = 1
export var max_coeff = 4
export var accel = -0.1
export var initial_speed = 600
export var damage = 1
export var knockback_override = 0
export var fall_start_coeff = 1.4
export var cluster = false
export var spinning = false
export var constant_fall = false
export var constant_fall_speed = 0
export var create_sound = ""
export var hit_sound = ""

var velocity = Vector2()
var velocity_scale = 0.0
var coeff = init_coeff
var y_distance_from_hitbox : float = 0
var fall_speed : float = 0
var gravity  : float = 1

var played = false
var knockback : float

# Called when the node enters the scene tree for the first time.
func _ready():
	if(knockback_override != 0):
		knockback = knockback_override
	else:
		knockback = damage
	var shadow = $AnimatedSprite.duplicate()
	shadow.name = "Shadow"
	shadow.modulate = "52000000"
	add_child(shadow)
	move_child(shadow,0)
func custom_effect(body): #Override me
	pass
	
func boom(body):
	if(body):
		body.hit(self)
		custom_effect(body)
	if(hit_sound != ""):
		State.play_sound(self,hit_sound)
	queue_free()

func _physics_process(delta):
	if(!played && create_sound != ""):
		State.play_sound(self,create_sound)
		played = true
	coeff += accel * delta
	coeff = max(0, min(coeff,max_coeff))
	
	if(spinning):
		$AnimatedSprite.rotation_degrees += delta*200 #200 looks nice I guess
		$Shadow.rotation_degrees += delta*200 #200 looks nice I guess
	velocity_scale =  max(delta * initial_speed * coeff, 0)
	var col = move_and_collide(velocity.normalized() * velocity_scale)
	#var current_y_dist : float = $AnimatedSprite.global_position.y - $HitBoxContainer/HitBox.global_position.y 
	if y_distance_from_hitbox:
		if(constant_fall):
			$AnimatedSprite.global_position.y = min($Shadow.global_position.y, $AnimatedSprite.global_position.y + constant_fall_speed * delta)
		elif coeff < (init_coeff / fall_start_coeff):
			var coeff_ratio = coeff / (init_coeff/fall_start_coeff)
			$AnimatedSprite.global_position.y = $Shadow.global_position.y + y_distance_from_hitbox * coeff_ratio
	else:
		y_distance_from_hitbox = $AnimatedSprite.global_position.y - $Shadow.global_position.y 
	
	bullet_range -= delta
	if(bullet_range < 0 || $AnimatedSprite.global_position.y > $Shadow.global_position.y - 0.01):
		boom(null)
		
func _on_Area2D_body_entered(body):
	if(hit_player && body.get_class() == "Player" ):
		boom(body)
	if(!hit_player && body.get_class() == "Enemy" ):
		boom(body)
	if(body.get_class() == "TileMap"):
		boom(null)
