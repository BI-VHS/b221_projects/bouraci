extends ColorRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var effect_pos;
var time = 0;
const MAX_TIME = 0.5;


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	set_position(Vector2.ZERO)
	set_size(get_viewport_rect().size)
	
	material.set_shader_param("screen_size", get_viewport_rect().size)
	
	var transform = get_tree().get_root().canvas_transform
	var global_position = transform.xform(effect_pos)
	material.set_shader_param("global_position", global_position)
	
func set_percent(percent):
	material.set_shader_param("percent", percent)

