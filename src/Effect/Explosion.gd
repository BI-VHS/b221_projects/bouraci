extends CanvasLayer

var time = 0
const MAX_TIME = 0.35

func set_effect_position(position):
	get_node("ColorRect").effect_pos = position


# Called when the node enters the scene tree for the first time.
func _ready():
	if(State.player.instance):
		State.player.instance.take_damage(25)
func _process(delta):
	time += delta
	if time > MAX_TIME:
		time = MAX_TIME
		get_parent().decrement_effects()
		queue_free()
		
	get_node("ColorRect").set_percent(time/MAX_TIME)
