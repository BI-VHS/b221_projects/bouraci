extends AudioStreamPlayer2D
func play_sound(sound, vol = 0):
	volume_db += vol
	stream = sound
	play()
func _on_AudioStreamPlayer2D_finished():
	queue_free()
