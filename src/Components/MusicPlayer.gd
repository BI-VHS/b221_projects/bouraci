extends AudioStreamPlayer

const music_mapping = {

	"res://src/Map/MapScenes/Demo/1_Village.tscn" : "res://assets/Sound/SFX/Music/village.mp3",
	"res://src/Map/MapScenes/Demo/1_VillageNoTut.tscn" : "res://assets/Sound/SFX/Music/village.mp3",
	"res://src/Map/MapScenes/Demo/2_Valley.tscn" : "res://assets/Sound/SFX/Music/forest.mp3",
	"res://src/Map/MapScenes/Demo/3_Cave.tscn" : "res://assets/Sound/SFX/Music/boss.mp3",
}

func play_sound(map, vol = 0):
	volume_db += vol
	stream = load(music_mapping[map])
	play()
func _on_AudioStreamPlayer_finished():
	queue_free()
