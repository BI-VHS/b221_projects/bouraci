extends Node2D

#CONVO OPTIONS: 
#click and (You) give item
#click and (You) receive item
#Change state - state determines the branch you start on. 
#Put the convo object into the _ready() function of children.

#examples:
#shop
#	options are shop items. 
#	No goto. Cost - price. Receive - what you buy. No state changes.
#quest
#	Give quest item. Set state to a reward branch.
#	Once reward received, set state to a "thank you again!!!" branch

var convo = { #Example convo.
	1 : {text:"Jak je?", choices:[	#Index from 1!!!!!!
			{text:"Fajn", goto:2},
			{text:"Naprd", goto:3},
		]},
	2 : {text:"To me tesi!"}, #That's it. Can retry the convo again.
	3 : {text:"To me mrzi, chces cigo?", choices:[
			{text:"Tak jo.", 
				goto:4, 
				receive:"res://src/Item/Consumable/Ciggy/Ciggy.tscn", 
				setstate:5,
				hover_text:"auto"},
			{text:"Nechces radsi pivko?", goto:6, give:"Beer", setstate:6},
		]},
	4 : {text:"Tady mas, uzivej."},
	5 : {text:"Dalsi cigo uz nemam, promin."}, #True dead end.
	6 : {text:"Dik."} #True dead end.
}

#CONVERSATION STUFF
var convo_state : int = 1 #Branch we start on.
var buttons = []
#To make your life a bit easier.
const text = "text"
const choices = "choices"		
const goto = "goto"				#do NOT set to 0
const setstate = "setstate"		#do NOT set to 0
const give = "give"
const receive = "receive"
const enabled_if = "enabled_if"
const show_if = "show_if"
const set_true = "set_true"
const set_false = "set_false"
const cost = "cost"
const hover_text = "hover_text"
const update_quest = "update_quest"

func update_dialogue(
	var index: int = 0, 
	var setstate: int = 0,
	var give: String = "",
	var receive: String = "",
	var set_true: String = "",
	var set_false: String = "",
	var update_quest: String = ""
	):
	
	if(give != ""):
		var to_give : Item = State.find_in_inventory(give)
		State.destroy_inventory_item(to_give)
	if(receive != ""):
		State.spawn_inventory_or_ground(receive, get_parent().global_position)
	if(set_true != ""):
		State.bools[set_true] = true
	if(set_false != ""):
		State.bools[set_true] = false
	if(update_quest != ""):
		State.ui.update_quest(update_quest)
	#changing state logic
	for b in buttons: #Remove all existing buttons.
		$DialogueText/Options.remove_child(b)
		if not is_instance_valid(b): #Wait for signals etc
			b.free()
	buttons = []
	$DialogueText.bbcode_text = convo[index].text

	if(convo[index].has(choices)):
	
		for choice in convo[index].choices:
			
			var button_show_if = choice.show_if if choice.has("show_if") else ""
			if(button_show_if && State.bools[button_show_if] == false):
				continue
			
			var child = Button.new()
			child.align = HALIGN_LEFT
			child.text = choice.text
			child.mouse_filter = Control.MOUSE_FILTER_STOP
			
			buttons.push_back(child)
			$DialogueText/Options.add_child(child)	
		
			var button_goto = choice.goto if choice.has("goto") else 0
			var button_setstate = choice.setstate if choice.has("setstate") else 0
			var button_give = choice.give if choice.has("give") else ""
			var button_receive = choice.receive if choice.has("receive") else ""
			var button_set_true = choice.set_true if choice.has("set_true") else ""
			var button_set_false = choice.set_false if choice.has("set_false") else ""
			var button_enabled_if = choice.enabled_if if choice.has("enabled_if") else ""
			var button_update_quest = choice.update_quest if choice.has("update_quest") else ""
			
			if(button_give):
				if(!State.find_in_inventory(button_give)):
					child.disabled = true

			if(button_enabled_if && State.bools[button_enabled_if] == false):
				child.disabled = true



			child.connect("pressed", self, "update_dialogue", [
				button_goto, button_setstate, button_give, 
				button_receive, button_set_true, button_set_false,
				button_update_quest
			])
			
			if choice.has(hover_text):
				var t = choice.hover_text
				if(t == "auto"): #MUST have receive
					var new_item = load(choice.receive).instance()
					t = new_item.get_node("ZHolder/ItemDescription").bbcode_text
					new_item.queue_free()
				child.connect("mouse_entered", self, "set_main_text", [t])
				child.connect("mouse_exited", self, "set_main_text", [convo[index].text])

	if(setstate):
		convo_state = setstate
		
func set_main_text(bbcode_text : String):
	$DialogueText.bbcode_text = bbcode_text
