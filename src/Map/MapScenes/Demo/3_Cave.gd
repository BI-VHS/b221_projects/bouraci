extends "res://src/Map/Map.gd"

onready var trog = $Nav/Walls/Trog
onready var pos1 = $Nav/Walls/Spawner1.global_position
onready var pos2 = $Nav/Walls/Spawner2.global_position
onready var pos3 = $Nav/Walls/Spawner3.global_position
onready var pos4 = $Nav/Walls/Spawner4.global_position
var goblin_res = preload("res://src/Enemy/_Old/Goblin/Goblin.tscn")
var zombie_res = preload("res://src/Enemy/_Old/Zombie/Zombie.tscn")

var goblin_timer = 0.0
var zombie_timer = 12.5

var won = false

var stopped_shooting = false
var old_cooldown = 0.0

func _physics_process(delta):
	if(!is_instance_valid(trog) && !won):
		won = true
		State.player.instance.set_physics_process(false)
		State.player.instance.get_node("AnimatedSprite").animation = "idle"
		for p in $Nav/Walls.get_children():
			if (p is AudioStreamPlayer):
				p.queue_free()
			if (p is Enemy):
				p.take_damage(99999)
		yield(get_tree().create_timer(2, false), "timeout")
		var info : PoolStringArray = ["Konec hry!", "Snad jste se bavili dobře.", "Pokud vás zajímá kde je ta koza, no, tady evidentně ne." ,"Přejeme hezký den."]
		State.ui.show_info(info)
		yield(get_tree().create_timer(2, false), "timeout")
		get_tree().quit()
	if(is_instance_valid(trog) && trog.chasing):
		goblin_timer += delta
		zombie_timer += delta
		
		trog.shoot_cooldown -= delta / 1.5
		trog.shoot_cooldown = max (trog.shoot_cooldown, 10)
		
#		if(State.player.stats.agility <= -10):
#			old_cooldown = trog.shoot_cooldown
#			trog.shoot_cooldown = INF
#			stopped_shooting = true
#		elif(stopped_shooting && State.player.stats.agility > -8):
#			trog.shoot_cooldown = old_cooldown
#			stopped_shooting = false
		trog.maxspeed += delta / 2
		trog.maxspeed = min ( trog.maxspeed, 150)

		if(goblin_timer > 10):
			var gob1 = goblin_res.instance()
			var gob2 = goblin_res.instance()
			$Nav/Walls.add_child(gob1)
			$Nav/Walls.add_child(gob2)
			gob1.global_position = pos1
			gob2.global_position = pos2
			goblin_timer -= 10

		if(zombie_timer > 25):
			var zob1 = zombie_res.instance()
			var zob2 = zombie_res.instance()
			var zob3 = zombie_res.instance()
			var zob4 = zombie_res.instance()
			$Nav/Walls.add_child(zob1)
			$Nav/Walls.add_child(zob2)
			$Nav/Walls.add_child(zob3)
			$Nav/Walls.add_child(zob4)
			zob1.global_position = pos1
			zob2.global_position = pos2
			zob3.global_position = pos3
			zob4.global_position = pos4
			zombie_timer -= 25
