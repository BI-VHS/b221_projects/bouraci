extends "res://src/Map/Map.gd"

onready var blocker_top = $Nav/Walls/Blocker1
onready var blocker_bottom = $Nav/Walls/Blocker2

func _ready():
	pass # Replace with function body.
func _on_Boss_death():
	yield(get_tree().create_timer(1, false), "timeout")
	blocker_top.queue_free()
func _on_Boss_aggro():
	yield(get_tree().create_timer(0.5, false), "timeout")
	blocker_bottom.visible = true
	blocker_bottom.get_node("CollisionShape2D").disabled = false
