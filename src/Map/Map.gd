extends Node2D

func _ready():
	State.walls = $Nav/Walls

var effect_amount = 0
const MAX_EFFECT_AMOUNT = 40

var effect_path = preload("res://src/Effect/Explosion.tscn")
func spawn_effect(position):
	if effect_amount < MAX_EFFECT_AMOUNT:
		print("spawned position", position, effect_amount)
		effect_amount += 1
		var effect = effect_path.instance()
		effect.set_effect_position(position)
		add_child(effect)
		

func decrement_effects():
	effect_amount -= 1


###
##MapGen
###
#
###
##Const
###
#const WIDTH  : int = 200
#const HEIGHT : int = 200
#
#const FLOOR : int = 1
#const WALL : int = 2
#const CAVE : int = 2
#const TESTER : int = 3
#
#const UP = Vector2(0,1)
#const DOWN = Vector2(0,-1)
#const LEFT = Vector2(-1,0)
#const RIGHT = Vector2(1,0)
#const NORTH = Vector2(0,1)
#const SOUTH = Vector2(0,-1)
#const WEST = Vector2(-1,0)
#const EAST = Vector2(1,0)
#const ORTHOGONAL = [UP, DOWN, LEFT, RIGHT]
#
#
#
#const diggable = [FLOOR, WALL, CAVE]
###
##Parameters
###
#export var END = Vector2(100,150)
#export(Array, Vector2) var STARTS = [Vector2(70,80), Vector2(100, 60), Vector2(130, 80)]
#
#export var SEED = 0
#
#export var CSIZE_L = 1	# The maximum cave size for this sector
#export var ERR_LOW = 0	# The navigation error probability
#export var HOR_LOW = 50	# The horizontal turn probability
#export var CAV_LOW = 0	# The probability of generating caves
#export var DIS_LOW = 0.2
#
#export var CSIZE_M = 6
#export var ERR_MID = 40
#export var HOR_MID = 60
#export var CAV_MID =  4
#export var DIS_MID=  0.5
#
#export var CSIZE_F = 8
#export var ERR_FAR = 25
#export var HOR_FAR = 25
#export var CAV_FAR =  4
#
#export var DISTANCE = 2
#export var MINCSIZE=  5
#
#export var BIO_COEF = 10
#export var BIO_CLOS = 20
#export var BIO_AWAY = 60
#export var BIO_CAVE = 5
#export var BIO_PROB = 40
#
#export var STRETCH_W = 1
#export var STRETCH_H = 2
###
##Glob
###
#var map_array = []
#
#var runners = []
#var branches = []
#var roomers = []
#
#var runnerlist = [runners, branches, roomers]
#var caves = []
#var floors = []
#var walls = []
#
#var biome_centers = []
#var rooms = []
#
#var rng : RandomNumberGenerator = null
#
###
##Class declarations
###
#const RUNNER = 0
#const BRANCH = 1
#const ROOMER = 2
#class Runner:
#	var pos = Vector2()
#	var dir : Vector2 = UP
#	var initdist
#	var dist
#	var counter
#	var countlist = 0
#class Branch extends Runner:
#	pass
#class Roomer extends Runner:
#	var size = 0
#
#class CaveRoom:
#	var center = Vector2()
#	var size = 0
#	var caves  = []
#	var cave_edges = []
#	var chokepoint_segments = []
#
###
##Utilities
###
#func map_get(var vec : Vector2) -> int:
#	if((vec.x >= 0 && vec.x < WIDTH) &&
#		(vec.y >= 0 && vec.y < HEIGHT)):
#		return map_array[vec.x][vec.y]
#	return -1
#
#func map_set(var vec : Vector2, var type : int) -> bool:
#	if((vec.x >= 0 && vec.x < WIDTH) &&
#		(vec.y >= 0 && vec.y < HEIGHT)):
#		if(type == FLOOR):
#			floors.append(vec)
#		map_array[vec.x][vec.y] = type
#		return true
#	return false
#
#func create_map_array(w, h):
#	var map = []
#	for x in range(w):
#		map.append([])
#		map[x].resize(h)
#		for y in range(w):
#			map[x][y] = WALL
#	return map
#
#func create_unset_array(w, h):
#	var map = []
#	for x in range(w):
#		map.append([])
#		map[x].resize(h)
#		for y in range(w):
#			map[x][y] = -1
#	return map
#
##percentual chance of something happening
#func prob(var p : float) -> bool:
#	return rng.randf() * 100 <= p
#func rand(var lo : int, var hi : int) -> int:
#	return rng.randi_range(lo, hi)
#func pick(var arr : Array):
#	return arr[rand(0, arr.size()-1)]
#func radius(var src : Vector2, var dist : int) -> Array:
#	var ret = []
#	for i in range(-dist, dist+1):
#		for j in range(-dist,dist+1):
#			ret.append(Vector2(src.x-i,src.y-j))
#	return ret
###
##Digging and moving
###
#func get_step(var src : Vector2, var dir : Vector2 ) -> Vector2:
#	var step = src + dir
#	if((step.x >= 0 && step.x < WIDTH) &&
#	  (step.y >= 0 && step.y < HEIGHT)):
#		#return map_array[step.x][step.y]
#		return step
#	return Vector2()
#
#func step(var runner : Runner, var dir : Vector2) -> bool:
#	var step = runner.pos + dir
#	if((step.x >= 0 && step.x < WIDTH) &&
#	  (step.y >= 0 && step.y < HEIGHT)):
#		runner.pos = step
#		return true
#	return false
#
#func can_dig(var t : int):
#	for d in diggable:
#		if(t == d):
#			return true
#	return false
##
#func dig(var runner : Runner, var length : int, var width : int = 1 ):
#	for i in length:
#		var step = get_step(runner.pos, runner.dir)
#		if(can_dig(map_get(step))):
#			map_set(step, FLOOR)
#			if(width > 1 && can_dig(map_get(get_step(runner.pos, runner.dir.rotated(deg2rad(90)))))):
#				map_set(get_step(runner.pos, runner.dir.rotated(deg2rad(90))), FLOOR)
#			if(width > 2 && can_dig(map_get(get_step(runner.pos, runner.dir.rotated(deg2rad(270)))))):
#				map_set(get_step(runner.pos, runner.dir.rotated(deg2rad(270))), FLOOR)
#			step(runner, runner.dir)
#
#func new_direction(var runner : Runner, var prob_err, var prob_hor) :
#	var x = runner.pos.x
#	var y = runner.pos.y
#	var newdir
#	if(!prob(prob_err)) :
#		if(prob(prob_hor)):
#			newdir = WEST if (x > END.x) else EAST
#		else:
#			newdir = SOUTH if (y > END.y) else NORTH
#	else:
#		if(prob(prob_hor)):
#			newdir = WEST if (x < END.x) else EAST
#		else:
#			newdir = SOUTH if (y > END.y) else NORTH
#	return newdir
#
#func run_direct(var runner : Runner):
#	runner.dir = WEST if (runner.pos.x > END.x)  else EAST
#	dig(runner, 1,2)
#func run_low(var runner : Runner):
#	runner.dir = new_direction(runner, ERR_LOW, HOR_LOW)
#	dig(runner,1,2)
#	if(prob(CAV_LOW)):
#		var roomer : Roomer = runner_new(runner.pos, ROOMER)
#		roomer.size = CSIZE_L
#func run_mid(var runner : Runner):
#	runner.dir = new_direction(runner, ERR_MID, HOR_MID)
#	dig(runner, rand(1,2), 2)
#	if(prob(CAV_MID)):
#		var roomer : Roomer = runner_new(runner.pos, ROOMER)
#		roomer.size = CSIZE_M
#	elif(prob(1)):
#		var branch : Branch = runner_new(runner.pos, BRANCH)
#func run_far(var runner : Runner):
#	runner.dir = new_direction(runner, ERR_FAR, HOR_FAR)
#	dig(runner, rand(1,3), 3)
#	if(prob(CAV_FAR)):
#		var roomer : Roomer = runner_new(runner.pos, ROOMER)
#		roomer.size = CSIZE_F
#	elif(prob(2)):
#		var branch : Branch = runner_new(runner.pos, BRANCH)
#
#func run_dumb(var runner : Runner, var initdir : Vector2):
#	if(initdir in [EAST, WEST, NORTH, SOUTH]):
#		if (prob(80)):
#			runner.dir = initdir
#		else:
#			runner.dir = pick([initdir.rotated(deg2rad(90)), initdir.rotated(deg2rad(270))])
#	else:
#		runner.dir = pick([initdir.rotated(deg2rad(45)), initdir.rotated(deg2rad(315))])
#	dig(runner, rand(1,3),rand(2,3))
#
###
###
##CLASS METHODS
###
###
##Dirty? Of course. But I want to access my globals.
#func runner_new(var pos : Vector2, var type : int):
#	var runner : Runner = null
#	match (type):
#		RUNNER:
#			runner = Runner.new()
#		BRANCH:
#			runner = Branch.new()
#		ROOMER:
#			runner = Roomer.new()
#	runner.pos = pos
#	runner.countlist = type
#	runnerlist[runner.countlist].append(runner)
#	return(runner)
#func runner_del(var runner : Runner):
#	runnerlist[runner.countlist].remove(runnerlist[runner.countlist].find(runner))
#	return
#func runner_trigger(var runner : Runner):
#	runner.initdist = runner.pos.distance_to(END)
#	runner.dist = runner.initdist
#	while(runner.dist > DISTANCE):
#		#new/obj/blue(loc) //todo add to main_path list, then check if a segment intersects. Those which intersect are marked as critical segments. For critical segments,\
#		#guarantee placement of a door. The door is to be placed on one of the tiles which intersects with the main path.
#		runner.dist = runner.pos.distance_to(END)
#		if(runner.dist < runner.initdist * DIS_LOW && END.y == runner.pos.y):
#			run_low(runner)
#			#run_direct(runner)
#		elif(runner.dist < runner.initdist * DIS_LOW):
#			run_low(runner)
#		elif(runner.dist < runner.initdist * DIS_MID):
#			run_mid(runner)
#		else:
#			run_far(runner)
#func branch_trigger(var brancher : Branch):
#	var walls = []
#	for T in radius(brancher.pos,1):
#		if(map_get(T) == WALL):
#			walls.append(T)
#	if(walls.empty()):
#		return
#	var wall = pick(walls)
#	var initdir = (wall - brancher.pos).normalized()
#	for i in rand(15,25):
#		run_dumb(brancher,initdir)
#func roomer_trigger(var roomer : Roomer):
#	var circlesize = rand(MINCSIZE, roomer.size)
#	for T in radius(roomer.pos,(circlesize*2)+1):
#		if(T in caves):
#			return
#	var angle   : int   = rand(0,359)
#	var stretch : float = rand(110,190)/100.0
#	var squish  : float = rand(110,190)/100.0
#	var cosA = cos(angle)
#	var sinA = sin(angle)
#	var A = ((pow(cosA,2))/pow(stretch,2)) + ((pow(sinA,2))/pow(squish,2))
#	var B = (2 * cosA * sinA * ((1/pow(stretch,2))-(1/pow(squish,2))))
#	var C = ((pow(sinA,2))/pow(stretch,2)) + ((pow(cosA,2))/pow(squish,2))
#	var new_room = CaveRoom.new()
#	rooms.append(new_room)
#	new_room.size = circlesize
#	new_room.center = roomer.pos
#	for T in radius(roomer.pos, circlesize*2):
#		if(can_dig(map_get(T))):
#			var x = roomer.pos.x
#			var y = roomer.pos.y
#			var eq : float = pow(A*(T.x - x),2) + B*(T.x - x)*(T.y - y) + pow(C*(T.y - y),2) - pow((circlesize/2.0),2)
#			if(eq < -2):
#				map_set(T,FLOOR)
#				caves.append(T)
#				new_room.caves.append(T)
#			if(eq <= -1.5 && eq >= - (circlesize *  2)) :
#				if (prob(5)):
#					var branch : Branch = runner_new(T, BRANCH)
#					new_room.caves.append(T)
#func find_walls(var f : Vector2):
#	for T in radius(f,1):
#		if(map_get(T) == WALL):
#			walls.append(T)
#func wall_trigger(var w : Vector2):
#	var neighbors = 0
#	for dir in ORTHOGONAL:
#		var tile = map_get(get_step(w, dir))
#		if (tile == WALL):
#			neighbors += 1
#	if (neighbors < 2):
#		map_set(w,FLOOR)
#func floor_trigger(var f : Vector2):
#	var neighbors = 0
#	for dir in ORTHOGONAL:
#		var tile = map_get(get_step(f, dir))
#		if (tile == FLOOR):
#			neighbors += 1
#	if (neighbors < 2):
#		map_set(f,WALL)
#####
#####
##Main
#####
#####
#func mapgen():
#	var time_start = OS.get_ticks_msec()
#	for start in STARTS:
#		runner_new(start, RUNNER)
#	for runner in runners:
#		runner_trigger(runner)
#	for runner in runners:
#		runner_del(runner)
#
#	for branch in branches:
#		branch_trigger(branch)
#	for branch in branches:
#		runner_del(branch)
#
#	for roomer in roomers:
#		roomer_trigger(roomer)
#	for roomer in roomers:
#		runner_del(roomer)
#
#	for branch in branches:
#		branch_trigger(branch)
#	for branch in branches:
#		runner_del(branch)
#
#	for cave_floor in floors:
#		find_walls(cave_floor)
#	for wall in walls:
#		wall_trigger(wall)
#	for cave_floor in floors:
#		floor_trigger(cave_floor)
#	print(walls.size())
#	print("Generation took ", OS.get_ticks_msec() - time_start,"ms")
#func _ready():
#	#Create map array
#
#	map_array = create_map_array(WIDTH,HEIGHT)
#
#	#Initialize the RNG
#	rng = RandomNumberGenerator.new()
#	if(SEED):
#		rng.seed = SEED
#	else:
#		rng.randomize()
#
#	#Map content
#	mapgen()
#
#	#Stretch map
#	var final = create_unset_array(WIDTH * STRETCH_W, HEIGHT * STRETCH_H)
##	for x in WIDTH:
##		for y in HEIGHT:
##			for i in STRETCH_W:
##				for j in STRETCH_H:
##					final[x * STRETCH_W + i][y * STRETCH_H + j] = map_array[x][y]
#	for v in walls:
#		for i in STRETCH_W:
#			for j in STRETCH_H:
#				final[v.x * STRETCH_W + i][v.y * STRETCH_H + j] = WALL
#	for v in floors:
#		for i in STRETCH_W:
#			for j in STRETCH_H:
#				final[v.x * STRETCH_W + i][v.y * STRETCH_H + j] = FLOOR
#	#Sets the map contents according to the array.
#	print("writing...")
#	for x in WIDTH * STRETCH_W:
#		for y in HEIGHT * STRETCH_H:
#
#				if(final[x][y] == WALL):
#					$Walls.set_cell(x,y,final[x][y])
#				if(final[x][y] == FLOOR):
#					$Nav/Ground.set_cell(x,y,final[x][y])
#	#Cleanup
#
#	$Walls/Player.position = $Walls.map_to_world(END * Vector2(STRETCH_W, STRETCH_H)+ Vector2(2,-2))
#
#	runnerlist.clear()
#	branches.clear()
#	roomers.clear()
#	#! The other arrays are to be used in the future.
