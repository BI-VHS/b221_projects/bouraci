extends Area2D


export var info : PoolStringArray = ["Zmen me, prosim!"]
export var single_use: bool = true

func _on_Area2D_body_entered(body):
	if body.name == "Player":
		State.ui.show_info(info)
		if(single_use): queue_free()
