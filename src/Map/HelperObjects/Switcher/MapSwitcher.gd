extends Node2D

# Zbytek je implementaccni ni detail!!!!!!!1
# Hrac na konkretni misto? DAlsi parametr pro signal kterej tam proste udela hrace,, 
#Stav muze klidnje hlidat "Game". A dame hraci jeho predmety a tak.
# klidne udelat loading na ctvrt vteriny at ta hra vypada ze ma tolik obsahu ze je co nacitat 8-)
# ale s tim kolik tam toho naserem neni vyloyuceny ze loading nejakej bude fakt nakeonc treba

signal stepped(target)

export var target = "res://src/Map/TotalDeathPlace.tscn"
export var new_text = ""
export var condition = ""

func _ready():
	connect("stepped", get_tree().get_root().get_node("Game/SceneLoader"),"_on_MapSwitcher_stepped") # T

func _on_Area2D_body_entered(body):
	if body.name == "Player":
		if(condition != "" && State.bools[condition] == false):
			return
			
		if(new_text != ""):
			State.ui.update_location(new_text)
		emit_signal("stepped", target)
