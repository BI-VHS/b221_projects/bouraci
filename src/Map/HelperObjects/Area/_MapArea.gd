extends Node2D

class_name MapArea

export var area_name : String = "Area"
#ONLY EVER resize the final instance, never any prototype
const extent : int = 100 #!!!
const step_size = 16
#Private
var extents : Vector2
var rows : int
var columns : int
var top_left : Vector2

func random_point() -> Vector2:
	var x = global_position.x + rand_range(-extent*scale.x, extent*scale.x)
	var y = global_position.y + rand_range(-extent*scale.y, extent*scale.y)
	return Vector2(x,y)
	
func next_point(start_point : Vector2) -> Vector2: #start_point is the GLOBAL position
	var start_point_relative = start_point - top_left
	var column : int= floor(start_point_relative.x / step_size)
	var row : int = floor(start_point_relative.y / step_size)
	var left = false
	var down = false
	var reset = false
	if(row%2 == 0):
		column += 1
		if(column >= columns):
			down = true
			row += 1
			if(row >= rows):
				reset = true
	else:
		column -= 1
		left = true
		if(column < 0):
			down = true
			row += 1
			if(row >= rows):
				reset = true
	if(reset):
		return (top_left + Vector2(rand_range(2,6), rand_range(2,6)))
	if(down):
		return(start_point + Vector2(0,step_size))
	if(left):
		return(start_point - Vector2(step_size,0))
	else:
		return(start_point + Vector2(step_size,0))
		
#func _process(delta):
#	$Sprite.global_position = random_point()

func _ready() -> void:

	extents = Vector2(extent*scale.x, extent*scale.y)
	rows = (2*extents.y)/step_size
	columns = (2*extents.x)/step_size
	top_left = Vector2(global_position - extents)
	$Label.visible = false
